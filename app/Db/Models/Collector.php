<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;
use App\Db\Models\Area;

class Collector extends Model
{
	protected $fillable =  ['name', 'cnic', 'address', 'family_size', 'contact_no', 'source_of_income', 'skills', 'reffered_by', 'need_cash', 'need_rashan', 'priority_id', 'area_id', 'zakat', 'other_cnic'];

	protected $casts = [
		'need_cash' => 'boolean',
		'need_rashan' => 'boolean',
		'zakat' => 'boolean',
    ];

	public function Area(){
		return $this->belongsTo(Area::class);
	}
}
