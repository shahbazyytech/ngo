<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
	protected $fillable =  ['name', 'qty', 'unit', 'rate', 'remarks', 'created_by'];

}
