<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;

class Vouchers extends Model
{
	protected $fillable =  ['name', 'amount', 'payment_mode', 'zakat', 'restricted', 'note', 'date'];

}
