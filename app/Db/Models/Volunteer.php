<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model
{
	protected $fillable =    ['name', 'cnic', 'address', 'contact_no', 'skills', 'area_id', 'age'];

	public function User(){
		return $this->belongsTo('App\Db\Models\User');
	}

	public function Area(){
		return $this->belongsTo('App\Db\Models\Area');
	}

	public function PacketsIssueToVolunteer(){
		return $this->hasMany('App\Db\Models\PacketsIssueToVolunteer');
	}
}
