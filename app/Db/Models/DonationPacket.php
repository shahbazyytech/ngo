<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;

class DonationPacket extends Model
{
	protected $fillable =    ['name', 'cash', 'rashan', 'amount', 'status', 'unit_cost', 'mazdoori', 'transportation', 'created_by', 'no_of_packets', 'available'];

	protected $casts = [
        'rashan' => 'object',
    ];

	public function User(){
		return $this->belongsTo('App\Db\Models\User', 'created_by');
	}

	public function PacketsIssueToVolunteer(){
		return $this->hasMany('App\Db\Models\PacketsIssueToVolunteer');
	}

	public function RashanPackets(){
		return $this->hasMany('App\Db\Models\RashanPacket', 'donation_packet_id');
	}

}
