<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
	protected $fillable =  ['id','province_id', 'city_name'];

	public function Province(){
		return $this->belongsTo('App\Db\Models\Province');
	}

	public function Areas(){
		return $this->hasMany('App\Db\Models\Area');
	}

}
