<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
	protected $fillable = ['item_id','inventory_voucher_id','qty', 'unit_cost', 'available', 'created_at'];


	public function scopeAvailable($query){
		return $query->where('available', '>', 0);
	}

	public function scopeFirstin($query){
		return $query->orderBy('created_at');
	}

	public function InventoryVoucher(){
		return $this->belongsTo('App\Db\Models\InventoryVoucher');
	}

	public function Item(){
		return $this->belongsTo('App\Db\Models\Item');
	}

}
