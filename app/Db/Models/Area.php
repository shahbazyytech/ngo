<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
	protected $fillable = ['parent_id','name','province_id','city_id', 'map_lat', 'map_lng'];
	
	public function InnerArea(){
		return $this->hasMany('App\Db\Models\Area', 'parent_id');
	}

	public function allInnerArea()
	{
		return $this->InnerArea()->with('allInnerArea');
	}

	public function Volunteers(){
		return $this->hasMany('App\Db\Models\Volunteer');
	}

	public function Province(){
		return $this->belongsTo('App\Db\Models\Province');
	}

	public function City(){
		return $this->belongsTo('App\Db\Models\City');
	}

}
