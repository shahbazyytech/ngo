<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;

class ExpenseVoucher extends Model
{
	protected $fillable =  ['amount', 'payment_mode', 'paid_by', 'expense_id','date'];

}
