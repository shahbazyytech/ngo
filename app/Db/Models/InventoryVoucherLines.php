<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;

class InventoryVoucherLines extends Model
{
	protected $fillable = ['item_id','voucher_id','qty', 'unit_cost'];
	
	public $timestamps = false;

	public function InventoryVoucher(){
		return $this->belongsTo('App\Db\Models\InventoryVoucher', 'voucher_id');
	}

	public function Item(){
		return $this->belongsTo('App\Db\Models\Item');
	}

}
