<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;

class PacketsIssueToCollector extends Model
{
	protected $fillable =    ['collector_id', 'no_of_packets', 'packets_issue_to_volunteer_id', 'collector_need_id', 'created_by'];

	public function User(){
		return $this->belongsTo('App\Db\Models\User', 'created_by');
	}

	public function Collector(){
		return $this->belongsTo('App\Db\Models\Collector');
	}

	public function CollectorNeed(){
		return $this->belongsTo('App\Db\Models\CollectorNeed');
	}
}
