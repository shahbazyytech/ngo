<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;
use App\Db\Models\Area;

class Investors extends Model
{
	protected $fillable =  ['name', 'cnic', 'future_contact', 'amount', 'payment_mod', 'contact_no', 'address'];

	protected $casts = [
		'future_contact' => 'boolean'
	];

}
