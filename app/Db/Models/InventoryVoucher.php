<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;

class InventoryVoucher extends Model
{
	protected $fillable = ['voucher_no', 'type', 'description','created_by', 'total_cost'];
	
	public function InventoryVoucherLines(){
		return $this->hasMany('App\Db\Models\InventoryVoucherLines', 'voucher_id');
	}

}
