<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
	protected $fillable =  ['id','name'];

	public function Cities(){
		return $this->hasMany('App\Db\Models\City');
	}

	public function Areas(){
		return $this->hasMany('App\Db\Models\Area');
	}

}
