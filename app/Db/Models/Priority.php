<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;
use App\Db\Models\CollectorNeed;

class Priority extends Model
{
	protected $fillable =    ['name', 'title', 'color'];
	
	public function CollectorNeed(){
		return $this->hasMany(CollectorNeed::class);
	}
}
