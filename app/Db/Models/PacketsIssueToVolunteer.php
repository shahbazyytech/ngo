<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;

class PacketsIssueToVolunteer extends Model
{
	protected $fillable =    ['volunteer_id', 'no_of_packets', 'donation_packet_id', 'available', 'created_by'];

	public function User(){
		return $this->belongsTo('App\Db\Models\User', 'created_by');
	}

	public function Volunteer(){
		return $this->belongsTo('App\Db\Models\Volunteer');
	}

	public function DonationPacket(){
		return $this->belongsTo('App\Db\Models\DonationPacket');
	}

	public function scopeAvailable($qry){
		return $qry->where('available', '>', 0);
	}

}
