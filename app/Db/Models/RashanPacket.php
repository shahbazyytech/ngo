<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;

class RashanPacket extends Model
{
	protected $fillable =    ['donation_packet_id', 'item_id', 'name', 'value', 'unit', 'unit_cost'];

	public $timestamps = false;

	public function DonationPacket(){
		return $this->belongsTo('App\Db\Models\DonationPacket');
	}

	public function Item(){
		return $this->belongsTo('App\Db\Models\Item');
	}

}
