<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;

class CollectorNeed extends Model
{
	protected $fillable =    ['collector_id', 'rashan', 'priority_id', 'status', 'cash', 'next_collection_date'];

	protected $casts = [
        'rashan' => 'boolean',
    ];

	protected static function boot(){
		parent::boot();

		static::creating(function ($query) {
			$query->next_collection_date = now();
		});
	}

	public function Priority(){
		return $this->hasMany('App\Db\Models\Priority');
	}

	public function Collector(){
		return $this->belongsTo('App\Db\Models\Collector');
	}
}
