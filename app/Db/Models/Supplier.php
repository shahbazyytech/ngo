<?php

namespace App\Db\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable =    ['user_id', 'name', 'qty', 'type_of_rashan', 'daily_supply_limit', 'timing', 'contact_no', 'address',  'map_lat', 'map_lng'];
}
