<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Db\Models\Supplier;

class SupplierController extends Controller
{

	public function __invoke()
	{
		return view('supplier.supplier', ['Suppliers' => Supplier::all()]);
	}

	public function SupplierForm()
	{
		return view('supplier.supplier_form');
	}

	public function CreateSupplier(Request $request){

		$request->validate([
			'name' => 'required',
			'address' => 'required',
		]);

		Supplier::create($request->only(['name', 'qty', 'type_of_rashan', 'daily_supply_limit', 'timing', 'contact_no',   'address',  'map_lat', 'map_lng']));
//		return dd($request->all());

		return redirect()->route('Supplier')->with(['success'	=> 'Data Updated!']);
	}

	public function GetEditSupplier($id){
		return view('supplier.supplier_edit', ['Supplier' => Supplier::findOrFail($id)]);
	}

	public function PostEditSupplier(Request $request, $id){

		$supplier = Supplier::findOrFail($id);
		$request->validate([
			'name' => 'required',
			'address' => 'required',
		]);

		$supplier->update($request->only(['name', 'qty', 'type_of_rashan', 'daily_supply_limit', 'timing', 'contact_no',   'address',  'map_lat', 'map_lng']));
		
		return redirect()->route('Supplier')->with(['success'	=> 'Data Updated!']);

	}

}
