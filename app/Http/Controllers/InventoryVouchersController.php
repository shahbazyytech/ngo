<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Db\Models\Inventory;
use App\Db\Models\Item;
use App\Db\Models\InventoryVoucher;
use App\Db\Models\InventoryVoucherLines;

class InventoryVouchersController extends Controller
{
	
	public function __invoke()
	{
		return view('inventory_vouchers.vouchers', ['Vouchers'	=> InventoryVoucher::all()]);
	}
	
	public function InventoryVoucherForm()
	{
		return view('inventory_vouchers.voucher_form', ['Items'	=>	Item::all()]);
	}

	public function CreateInventoryVoucher(Request $request){
		$request->validate([
			'voucher_no' => 'required',
			'type' => 'required|in:receive',
//			'type' => 'required|in:receive,donate',
			'total_cost' => 'required_if:type,receive',
			'items.*.item_id'	=>	'required|integer',
			'items.*.qty'	=>	'required|integer',
//			'items.*.rate'	=>	'required',
			'items.*.unit_cost'	=>	'required_if:type,receive'
		]);

//		dd($request->all());

		$InventoryVoucher =	InventoryVoucher::create($request->only(['voucher_no', 'description', 'type', 'total_cost']));
		$items	=	collect($request->input('items'))->map(function($item) use ($InventoryVoucher){
			return [ 'item_id'	=> $item['item_id'], 'qty'	=>	$item['qty'], 'voucher_id'	=>	$InventoryVoucher->id, 'unit_cost'	=>	$item['unit_cost']];
		});

		InventoryVoucherLines::insert($items->toArray());

		if($request->input('type') == 'receive'){
			foreach ($request->input('items') as $row) {
				Inventory::create(['item_id'	=> $row['item_id'], 'qty'	=>	$row['qty'], 'inventory_voucher_id'	=>	$InventoryVoucher->id, 'unit_cost'	=>	$row['unit_cost'], 'available'	=>	$row['qty']]);
			}
		}

		foreach ($request->input('items') as $item) {
			Item::where('id', $item['item_id'])->update(['qty' => $item['updated_qty']]);
		}
		return redirect()->route('InventoryVouchers')->with(['success' => 'Data Updated!']);
	}

	public function GetEditInventoryVoucher($id){

		$inventory = Inventory::where('inventory_voucher_id', $id)->whereRaw('available != qty')->get();
		if($inventory->count()){
			return redirect()->route('InventoryVouchers')->withErrors(['Voucher Quantity in used']);
		}

		$Voucher 	=	InventoryVoucher::findOrFail($id);
		$SelectedItems = InventoryVoucherLines::with('Item')->where('voucher_id',  $Voucher->id)->get();
		$SelectedItems = $SelectedItems->map(function($item) use ($Voucher){
			return [
				'id'	=>	$item->item_id,
				'name'	=>	$item->Item->name,
				'qty'	=>	($Voucher->type == 'receive')? ($item->Item->qty - $item->qty) : ($item->Item->qty + $item->qty),
				'unit'	=>	$item->Item->unit,
				'input_qty'	=>	$item->qty,
				'unit_cost'	=>	$item->unit_cost,
			];
		});
		return view('inventory_vouchers.voucher_edit', ['Items'	=>	Item::all(), 'Voucher' => $Voucher, 'SelectedItems' => $SelectedItems]);
	}

	public function PostEditInventoryVoucher(Request $request, $id){

		$request->validate([
			'voucher_no' => 'required',
			'total_cost' => 'required_if:type,receive',
			'items.*.item_id'	=>	'required|integer',
			'items.*.qty'	=>	'required|integer',
			'items.*.unit_cost'	=>	'required_if:type,receive'
		]);

//		dd($request->all());

		$InventoryVoucher =	InventoryVoucher::with(['InventoryVoucherLines'	=>	function($qry){
			$qry->with('Item');
		}])->findOrFail($id);

		$InventoryVoucher->description = $request->input('description');
		$InventoryVoucher->voucher_no = $request->input('voucher_no');
		$InventoryVoucher->total_cost = $request->input('total_cost');
		$InventoryVoucher->save();

		foreach($InventoryVoucher->InventoryVoucherLines as $VoucherLine){
			$VoucherLine->Item->qty = ($InventoryVoucher->type == 'receive')? ($VoucherLine->Item->qty - $VoucherLine->qty) : ($VoucherLine->Item->qty + $VoucherLine->qty);
			$VoucherLine->Item->save();
		}

		$items	=	collect($request->input('items'))->map(function($item) use ($id){
			return [ 'item_id'	=> $item['item_id'], 'qty'	=>	$item['qty'], 'voucher_id'	=>	$id, 'unit_cost'	=>	$item['unit_cost']];
		});

		InventoryVoucherLines::where('voucher_id', $id)->delete();
		Inventory::where('inventory_voucher_id', $id)->delete();

		InventoryVoucherLines::insert($items->toArray());

		if($InventoryVoucher->type == 'receive'){
			foreach ($request->input('items') as $row) {
				Inventory::create([
					'item_id'	=> $row['item_id'],
					'qty'	=>	$row['qty'],
					'inventory_voucher_id'	=>	$InventoryVoucher->id,
					'unit_cost'	=>	$row['unit_cost'],
					'available'	=>	$row['qty'],
					'created_at'	=>	$InventoryVoucher->created_at,
					]);
			}
		}

		foreach ($request->input('items') as $item) {
			Item::where('id', $item['item_id'])->update(['qty' => $item['updated_qty']]);
		}
		return redirect()->route('InventoryVouchers')->with(['success' => 'Data Updated!']);

	}

}
