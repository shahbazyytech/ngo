<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Db\Models\DonationPacket;
use App\Db\Models\CollectorNeed;
use App\Db\Models\Province;
use App\Db\Models\City;
use App\Db\Models\Area;
use App\Db\Models\Volunteer;
use App\Db\Models\Collector;
use App\Db\Models\PacketsIssueToVolunteer;
use App\Db\Models\PacketsIssueToCollector;
use DB;

class PacketsIssueController extends Controller
{

	public function GetPacketsIssueVolunteer(){
		return view('packet_issue.packet_issue', [
			'Provinces' => Province::all(),
			'Cities'	=>	City::all(),
			'Areas' => Area::with('InnerArea')->where('parent_id', null)->get(),
			'Volunteers' => Volunteer::all(),
/* 			'Collectors'	=>	Collector::all(), */
/* 			'VolunteerPackets'	=>	PacketsIssueToVolunteer::where('available', '>', 0)->get(), */
			'Packets' => DonationPacket::all(),
			]);
	}

	public function PostPacketsIssueVolunteer(Request $request){

		$request->validate([
			'volunteer_id' => 'required|integer|exists:volunteers,id',
			'packets.*.donation_packet_id' => 'required|integer|exists:donation_packets,id',
			'packets.*.no_of_packets' => 'required|integer',
		]);
	
		$DonationPackets = DonationPacket::whereIn('id', $request->input('packets.*.donation_packet_id'))->get();

		foreach ($DonationPackets as $key => $packet) {
			$collect	=	collect($request->input('packets'));
			$req = $collect->firstWhere('donation_packet_id', $packet->id);
			if($packet->available < $req['no_of_packets']){
				return redirect()->back()->withErrors(['error' => 'Available packet is lessthan are issued to volunteers packets!', 'hint' => 'You can change no_of_packets']);
			}
		}

		foreach ($DonationPackets as $key => $packet) {

			$collect	=	collect($request->input('packets'));
			$req = $collect->firstWhere('donation_packet_id', $packet->id);

			$PacketsIssueToVolunteer	=	PacketsIssueToVolunteer::where([
				'volunteer_id'	=>	 $request->input('volunteer_id'),
				'donation_packet_id'	=>	$req['donation_packet_id'],
			])->first();
			if($PacketsIssueToVolunteer){
				$PacketsIssueToVolunteer->update([
					'no_of_packets'	=>	($req['no_of_packets'] + $PacketsIssueToVolunteer->no_of_packets),
					'available' => ($req['no_of_packets'] + $PacketsIssueToVolunteer->available),
					]);
			} else {
				PacketsIssueToVolunteer::create([
					'volunteer_id'	=>	 $request->input('volunteer_id'),
					'no_of_packets'	=>	$req['no_of_packets'],
					'donation_packet_id'	=>	$req['donation_packet_id'],
					'available' => $req['no_of_packets']
					]);
			}

			$packet->available = ($packet->available - $req['no_of_packets']);
			$packet->save();
		}

		return redirect()->route('GetPacketsIssueVolunteer')->with(['success' => 'Data Updated!']);

	}

	public function GetPacketsIssueCollector(){
		return view('packet_issue.packet_issue_collector', [
			'Provinces' => Province::all(),
			'Cities'	=>	City::all(),
			'Areas' => Area::with('InnerArea')->where('parent_id', null)->get(),
			]);
	}

	public function GetPacketsIssueCollectorData(Request $request){
		$data = [
			'Volunteers' => Volunteer::where('area_id', $request->input('area_id'))->get(),
			'Collectors' => DB::table('collectors')
							->select('collectors.*', 'collector_needs.id as collector_need_id', 'collector_needs.status as collector_need_status')
							->join('collector_needs', 'collectors.id', '=', 'collector_needs.collector_id')
							->where('collectors.area_id', $request->input('area_id'))
							->where('collector_needs.status', 'pending')
							->where('next_collection_date', '<=', now())
							->get(),
		];
		$data['VolunteerPackets']	=	PacketsIssueToVolunteer::with('DonationPacket')->whereIn('volunteer_id', $data['Volunteers']->pluck('id'))->where('available', '>', 0)->get();
		return $data;
	}

	public function PostPacketsIssueCollector(Request $request){
//		echo now()->addWeek($request->input);
//		dd($request->all());

		$request->validate([
			'collector_id' => 'required|integer|exists:collectors,id',
			'collector_need_id'	=>	'required|integer|exists:collector_needs,id',
			'packets_issue_to_volunteer_id' => 'required|integer|exists:packets_issue_to_volunteers,id',
			'next_collection_in'	=>	'required',
			'no_of_packets' => 'required|integer',
		]);

		$PacketIssueToVolunteer = PacketsIssueToVolunteer::find($request->input('packets_issue_to_volunteer_id'));

		if($PacketIssueToVolunteer->available < $request->input('no_of_packets')){
			return redirect()->back()->withErrors(['error' => 'Available packet is lessthan are issued to collector packets!', 'hint' => 'You can change no_of_packets']);
		}

		PacketsIssueToCollector::create($request->only([
			'collector_id', 'no_of_packets', 'packets_issue_to_volunteer_id', 'collector_need_id'
		]));
		
		$PacketIssueToVolunteer->available = ($PacketIssueToVolunteer->available - $request->input('no_of_packets'));
		$PacketIssueToVolunteer->save();

		if($request->input('next_collection_in') == 'completed'){
			CollectorNeed::where('id', $request->input('collector_need_id'))->update(['status'	=>	'completed']);
		} else {
			CollectorNeed::where('id', $request->input('collector_need_id'))->update(['next_collection_date'	=>	now()->addWeek($request->input('next_collection_in'))]);
		}

		return redirect()->route('GetPacketsIssueCollector')->with(['success' => 'Data Updated!']);

	}

	public function PackestIssueToCollector(Request $request){

		$request->validate([
			'collector_id' => 'required|integer|exists:collectors,id',
			'packets.*.packets_issue_to_volunteer_id' => 'required|integer|exists:packets_issue_to_volunteers,id',
			'packets.*.collector_need_id'	=>	'required|integer|exists:collector_needs,id',
			'packets.*.collector_need_status'	=>	'required|in:completed,pending',
			'packets.*.no_of_packets' => 'required|integer',
		]);

		$PacketsIssueToVolunteer = PacketsIssueToVolunteer::whereIn('id', $request->input('packets.*.packets_issue_to_volunteer_id'))->get();

		foreach ($PacketsIssueToVolunteer as $key => $packet) {
			$req = collect($request->input('packets'))->firstWhere('packets_issue_to_volunteer_id', $packet->id);
			if($packet->available < $req['no_of_packets']){
				return response()->json(['error' => 'Available packet is lessthan are issued to Collector packets!', 'hint' => 'You can change no_of_packets'], 400, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
			}
		}

		foreach ($PacketsIssueToVolunteer as $key => $packet) {
			$req = collect($request->input('packets'))->firstWhere('packets_issue_to_volunteer_id', $packet->id);
			PacketsIssueToCollector::create([
				'collector_id'	=>	$request->input('collector_id'),
				'no_of_packets'	=>	$req['no_of_packets'],
				'packets_issue_to_volunteer_id'	=>	$req['packets_issue_to_volunteer_id'],
				'collector_need_id'	=>	$req['collector_need_id'],
				'available' => $req['no_of_packets']
			]);

			$packet->available = ($packet->available - $req['no_of_packets']);
			$packet->save();

			CollectorNeed::where('id', $packet['collector_need_id'])->update(['status'	=>	$packet['collector_need_status']]);

		}
	
		return response()->json(['success' => 'Packets issued to collector!'], 201, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);

	}

}
