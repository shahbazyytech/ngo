<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Db\Models\DonationPacket;

class DonationPacketController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$request->validate([
			'per_page' => 'sometimes|integer|max:50',
		]);
		$per_page = $request->has('per_page')? $request->input('per_page') : 15;
		return DonationPacket::paginate($per_page);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$request->validate([
			'name' => 'required',
			'status' => 'required|in:completed,available',
			'no_of_packets'	=> 'required|integer',
			'rashan'	=>	'required_without:cash',
			'cash'	=>	'required_without:rashan'
		]);

		DonationPacket::create($request->only([
			'name', 'cash', 'rashan', 'status', 'no_of_packets', 
		]) + ['available' => $request->input('no_of_packets')]);

		return response()->json(['success' => 'DonationPacket Created!'], 201, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		return response()->json(DonationPacket::findOrFail($id), 200, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{

		$DonationPacket =	DonationPacket::findOrFail($id);

		if($request->has('update_packets')){
			$request->validate([
				'name' => 'required',
				'update_packets'	=> 'required|min:'.($donationPacket->no_of_packets - $DonationPacket->available),
			]);

			$DonationPacket->update(['no_of_packets' => $request->input('update_packets'), 'available' => ($request->input('update_packets') - ($DonationPacket->no_of_packets - $DonationPacket->available))]);

			return response()->json(['success' => 'DonationPacket Updated!'], 201, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
		}
		$request->validate([
			'name' => 'required',
			'no_of_packets'	=> 'required|integer',
			'rashan'	=>	'required_without:cash',
			'cash'	=>	'required_without:rashan'
		]);
		if($DonationPacket->available == $DonationPacket->no_of_packets){
			$DonationPacket->update($request->only([
				'name', 'cash', 'rashan', 'status', 'no_of_packets', 
			]) + ['available' => $request->input('no_of_packets')]);
			return response()->json(['success' => 'DonationPacket Updated!'], 201, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
		} else {
			return response()->json(['error' => 'DonationPacket are issued to volunteers!', 'hint' => 'You can change no_of_packets'], 400, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id)
	{
		DonationPacket::destroy($id);
		return response()->json(['success' => 'DonationPacket Deleted!'], 201, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
	}
}
