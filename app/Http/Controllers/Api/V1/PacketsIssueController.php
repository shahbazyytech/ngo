<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Db\Models\DonationPacket;
use App\Db\Models\CollectorNeed;
use App\Db\Models\PacketsIssueToVolunteer;
use App\Db\Models\PacketsIssueToCollector;

class PacketsIssueController extends Controller
{

	public function PacketIssueToVolunteer(Request $request){

		$request->validate([
			'volunteer_id' => 'required|integer|exists:volunteers,id',
			'donation_packet_id' => 'required|integer|exists:donation_packets,id',
			'no_of_packets' => 'required|integer',
		]);
	
		$DonationPacket = DonationPacket::find($request->input('donation_packet_id'));

		if($DonationPacket->available < $request->input('no_of_packets')){
			return response()->json(['error' => 'Available packet is lessthan are issued to volunteers packets!', 'hint' => 'You can change no_of_packets'], 400, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
		}

		$PacketsIssueToVolunteer	=	PacketsIssueToVolunteer::where($request->only(['volunteer_id', 'donation_packet_id']))->first();
		if($PacketsIssueToVolunteer){
			$PacketsIssueToVolunteer->update([
				'no_of_packets'	=>	($request->input('no_of_packets') + $PacketsIssueToVolunteer->no_of_packets),
				'available'	=>	($request->input('available') + $PacketsIssueToVolunteer->available),
				]);
		} else {
			PacketsIssueToVolunteer::create($request->only([
				'volunteer_id', 'no_of_packets', 'donation_packet_id'
			]) + ['available' => $request->input('no_of_packets')]);
		}
		
		$DonationPacket->available = ($DonationPacket->available - $request->input('no_of_packets'));
		$DonationPacket->save();

		return response()->json(['success' => 'Packets issued to volunteer!'], 201, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);

	}

	public function PacketsIssueToVolunteer(Request $request){

		$request->validate([
			'volunteer_id' => 'required|integer|exists:volunteers,id',
			'packets.*.donation_packet_id' => 'required|integer|exists:donation_packets,id',
			'packets.*.no_of_packets' => 'required|integer',
		]);
	
		$DonationPackets = DonationPacket::whereIn('id', $request->input('packets.*.donation_packet_id'))->get();

		foreach ($DonationPackets as $key => $packet) {
			$collect	=	collect($request->input('packets'));
			$req = $collect->firstWhere('donation_packet_id', $packet->id);
			if($packet->available < $req['no_of_packets']){
				return response()->json(['error' => 'Available packet is lessthan are issued to volunteers packets!', 'hint' => 'You can change no_of_packets'], 400, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
			}
		}

		foreach ($DonationPackets as $key => $packet) {
			
			$collect	=	collect($request->input('packets'));
			$req = $collect->firstWhere('donation_packet_id', $packet->id);

			$PacketsIssueToVolunteer	=	PacketsIssueToVolunteer::where([
				'volunteer_id'	=>	 $request->input('volunteer_id'),
				'donation_packet_id'	=>	$req['donation_packet_id'],
			])->first();
			if($PacketsIssueToVolunteer){
				$PacketsIssueToVolunteer->update([
					'no_of_packets'	=>	($req['no_of_packets'] + $PacketsIssueToVolunteer->no_of_packets),
					'available' => ($req['no_of_packets'] + $PacketsIssueToVolunteer->available),
					]);
			} else {
				PacketsIssueToVolunteer::create([
					'volunteer_id'	=>	 $request->input('volunteer_id'),
					'no_of_packets'	=>	$req['no_of_packets'],
					'donation_packet_id'	=>	$req['donation_packet_id'],
					'available' => $req['no_of_packets']
					]);
			}



			$packet->available = ($packet->available - $req['no_of_packets']);
			$packet->save();
		}

		return response()->json(['success' => 'Packets issued to volunteer!'], 201, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);

	}

	public function PacketIssueToCollector(Request $request){

		$request->validate([
			'collector_id' => 'required|integer|exists:collectors,id',
			'packets_issue_to_volunteer_id' => 'required|integer|exists:Packets_issue_to_volunteers,id',
			'collector_need_id'	=>	'required|integer|exists:collector_needs,id',
			'collector_need_status'	=>	'required|in:completed,pending',
			'no_of_packets' => 'required|integer',
		]);

		$PacketIssueToVolunteer = PacketsIssueToVolunteer::find($request->input('packets_issue_to_volunteer_id'));

		if($PacketIssueToVolunteer->available < $request->input('no_of_packets')){
			return response()->json(['error' => 'Available packet is lessthan are issued to Collector packets!', 'hint' => 'You can change no_of_packets'], 400, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
		}

		PacketsIssueToCollector::create($request->only([
			'collector_id', 'no_of_packets', 'packets_issue_to_volunteer_id', 'collector_need_id'
		]) + ['available' => $request->input('no_of_packets')]);
		
		$PacketIssueToVolunteer->available = ($PacketIssueToVolunteer->available - $request->input('no_of_packets'));
		$PacketIssueToVolunteer->save();

		CollectorNeed::where('id', $request->input('collector_need_id'))->update(['status'	=>	$request->input('collector_need_status')]);

		return response()->json(['success' => 'Packets issued to collector!'], 201, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);

	}

	public function PackestIssueToCollector(Request $request){

		$request->validate([
			'collector_id' => 'required|integer|exists:collectors,id',
			'packets.*.packets_issue_to_volunteer_id' => 'required|integer|exists:Packets_issue_to_volunteers,id',
			'packets.*.collector_need_id'	=>	'required|integer|exists:collector_needs,id',
			'packets.*.collector_need_status'	=>	'required|in:completed,pending',
			'packets.*.no_of_packets' => 'required|integer',
		]);

		$PacketsIssueToVolunteer = PacketsIssueToVolunteer::whereIn('id', $request->input('packets.*.packets_issue_to_volunteer_id'))->get();

		foreach ($PacketsIssueToVolunteer as $key => $packet) {
			$req = collect($request->input('packets'))->firstWhere('packets_issue_to_volunteer_id', $packet->id);
			if($packet->available < $req['no_of_packets']){
				return response()->json(['error' => 'Available packet is lessthan are issued to Collector packets!', 'hint' => 'You can change no_of_packets'], 400, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
			}
		}

		foreach ($PacketsIssueToVolunteer as $key => $packet) {
			$req = collect($request->input('packets'))->firstWhere('packets_issue_to_volunteer_id', $packet->id);
			PacketsIssueToCollector::create([
				'collector_id'	=>	$request->input('collector_id'),
				'no_of_packets'	=>	$req['no_of_packets'],
				'packets_issue_to_volunteer_id'	=>	$req['packets_issue_to_volunteer_id'],
				'collector_need_id'	=>	$req['collector_need_id'],
				'available' => $req['no_of_packets']
			]);

			$packet->available = ($packet->available - $req['no_of_packets']);
			$packet->save();

			CollectorNeed::where('id', $packet['collector_need_id'])->update(['status'	=>	$packet['collector_need_status']]);

		}
	
		return response()->json(['success' => 'Packets issued to collector!'], 201, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);

	}

}
