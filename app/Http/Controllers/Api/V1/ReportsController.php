<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Db\Models\Area;
use App\Db\Models\City;
use App\Db\Models\Province;
use App\Db\Models\Volunteer;


class ReportsController extends Controller
{
	public function AreaWise(Request $request){

		$request->validate([
			'province_id' => 'required_without_all:city_id,area_id|exists:provinces,id',
			'city_id' => 'required_without_all:province_id,area_id|exists:cities,id',
			'area_id' => 'required_without_all:city_id,province_id|exists:areas,id',
		]);

		if($request->has('province_id')){

			$data =	Province::with(['Cities'	=>	function($qry){
				return $qry->with(['Areas'	=>	function($qry){
					return $qry->with('Volunteers');
				}]);
			}])->find($request->input('province_id'));

		} else if($request->has('city_id')){

			$data =	City::with(['Areas'	=>	function($qry){
				return $qry->with('Volunteers');
			}])->find($request->input('city_id'));

		} else if($request->has('area_id')){

			$data =	Area::with('Volunteers')->find($request->input('area_id'));

		}

		return response()->json($data, 200, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
//		return response()->json(['error' => 'request Not completed', 'hint' => 'You can change no_of_packets'], 400, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);

	}

	public function PacketIssueStatus(Request $request){
		$request->validate([
			'province_id' => 'required_without_all:city_id,area_id|exists:provinces,id',
			'city_id' => 'required_without_all:province_id,area_id|exists:cities,id',
			'area_id' => 'required_without_all:city_id,province_id|exists:areas,id',
		]);

		if($request->has('province_id')){

			$data =	Province::with(['Cities'	=>	function($qry){
				return $qry->with(['Areas'	=>	function($qry){
					return $qry->with(['Volunteers'	=>	function($qry){
						return $qry->with(['PacketsIssueToVolunteer' => function($qry){
							return $qry->select('id', 'volunteer_id', 'available', 'no_of_packets')->Available();
						}]);
					}]);
				}]);
			}])->find($request->input('province_id'));

		} else if($request->has('city_id')){

			$data =	City::with(['Areas'	=>	function($qry){
				return $qry->with(['Volunteers'	=>	function($qry){
					return $qry->with(['PacketsIssueToVolunteer' => function($qry){
						return $qry->select('id', 'volunteer_id', 'available', 'no_of_packets')->Available();
					}]);
				}]);
			}])->find($request->input('city_id'));

		} else if($request->has('area_id')){

			$data =	Area::with(['Volunteers'	=>	function($qry){
				return $qry->with(['PacketsIssueToVolunteer' => function($qry){
					return $qry->select('id', 'volunteer_id', 'available', 'no_of_packets')->Available();
				}]);
			}])->find($request->input('area_id'));

		}

		return response()->json($data, 200, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);

	}


}
