<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Db\Models\CollectorNeed;

class CollectorNeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$request->validate([
			'per_page' => 'sometimes|integer|max:50',
		]);
		$per_page = $request->has('per_page')? $request->input('per_page') : 15;
		return CollectorNeed::with('Collector')->paginate($per_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$request->validate([
			'collector_id' => 'required|integer|exists:collectors,id',
//			'priority' => 'required|in:high,normal,low',
			'priority_id'	=>	'required',
			'rashan'	=>	'required_without:cash',
			'cash'	=>	'required_without:is_rashan'
		]);

		CollectorNeed::create($request->only([
			'collector_id', 'priority', 'rashan', 'cash', 'priority_id'
		]) + ['status'	=>	'pending']);

		return response()->json(['success' => 'CollectorNeed Created!'], 201, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		return response()->json(CollectorNeed::with('Collector')->findOrFail($id), 200, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

		$CollectorNeed = CollectorNeed::findOrFail($id);

		$request->validate([
			'collector_id' => 'required|integer|exists:collectors,id',
//			'priority' => 'required|in:high,normal,low',
			'priority_id'	=>	'required',
			'rashan'	=>	'required_without:cash',
			'cash'	=>	'required_without:is_rashan'
		]);

		$CollectorNeed->update($request->only([
			'collector_id', 'priority', 'rashan', 'cash', 'priority_id'
		]) + ['status'	=>	'pending']);

		return response()->json(['success' => 'CollectorNeed Updated!'], 201, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		CollectorNeed::destroy($id);
		return response()->json(['success' => 'CollectorNeed Deleted!'], 201, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
    }
}
