<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Db\Models\City;
use App\Db\Models\Priority;
use App\Db\Models\Collector;
use App\Db\Models\Investors;
use App\Db\Models\Volunteer;
use App\Db\Models\Vouchers;
use App\Db\Models\ExpenseVoucher;
use App\Db\Models\InventoryVoucher;
use App\Db\Models\PacketsIssueToCollector;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Carbon\Carbon;

class DashboardController extends Controller
{

    protected $data;

    public function __invoke(){

        $data['CollectorCount'] = Collector::count();
		$data['VolunteersCount'] = Volunteer::count();
		$data['DonorsCount'] = Investors::count();
		$data['ActiveNeedCount']	=	Collector::join('collector_needs', 'collectors.id', '=', 'collector_needs.collector_id')
										->where('collector_needs.status', 'pending')
										->where('collector_needs.next_collection_date', '<=', now()->toDateString())
										->count();
		$data['SumExpenses'] = ExpenseVoucher::where('date', '>=', now()->subMonth(1)->toDateString())->sum('amount');
		$data['SumDonationReceive'] = Vouchers::where('created_at', '>=', now()->subMonth(1)->toDateString())->sum('amount');
		$data['SumInventoryReceived'] = InventoryVoucher::where('created_at', '>=', now()->subMonth(1)->toDateString())->where('type', 'receive')->sum('total_cost');
		$data['MonthlyDonation'] = Vouchers::select(DB::raw('SUM(`amount`) as amount, MONTHNAME(`date`) AS `month`, YEAR(`date`) as year'))
				->groupBy(DB::raw('MONTHNAME(`date`)'))
				->groupBy(DB::raw('YEAR(`date`)'))
				->get();

		$data['MonthlyDonation'] = Vouchers::select(DB::raw('
				SUM( IF( MONTH(`date`) = 1, `amount`, 0) ) AS `Jan`,
				SUM( IF( MONTH(`date`) = 2, `amount`, 0) ) AS `Feb`,
				SUM( IF( MONTH(`date`) = 3, `amount`, 0) ) AS `Mar`,
				SUM( IF( MONTH(`date`) = 4, `amount`, 0) ) AS `Apr`,
				SUM( IF( MONTH(`date`) = 5, `amount`, 0) ) AS `May`,
				SUM( IF( MONTH(`date`) = 6, `amount`, 0) ) AS `Jun`,
				SUM( IF( MONTH(`date`) = 7, `amount`, 0) ) AS `Jul`,
				SUM( IF( MONTH(`date`) = 8, `amount`, 0) ) AS `Aug`,
				SUM( IF( MONTH(`date`) = 9, `amount`, 0) ) AS `Sep`,
				SUM( IF( MONTH(`date`) = 10, `amount`, 0) ) AS `Oct`,
				SUM( IF( MONTH(`date`) = 11, `amount`, 0) ) AS `Nov`,
				SUM( IF( MONTH(`date`) = 12, `amount`, 0) ) AS `Dec`
			'))
			->where(DB::raw('YEAR(`date`)'), '=', date('Y'))
			->first();

		$data['MonthlyExpense'] = ExpenseVoucher::select(DB::raw('
			SUM( IF( MONTH(`date`) = 1, `amount`, 0) ) AS `Jan`,
			SUM( IF( MONTH(`date`) = 2, `amount`, 0) ) AS `Feb`,
			SUM( IF( MONTH(`date`) = 3, `amount`, 0) ) AS `Mar`,
			SUM( IF( MONTH(`date`) = 4, `amount`, 0) ) AS `Apr`,
			SUM( IF( MONTH(`date`) = 5, `amount`, 0) ) AS `May`,
			SUM( IF( MONTH(`date`) = 6, `amount`, 0) ) AS `Jun`,
			SUM( IF( MONTH(`date`) = 7, `amount`, 0) ) AS `Jul`,
			SUM( IF( MONTH(`date`) = 8, `amount`, 0) ) AS `Aug`,
			SUM( IF( MONTH(`date`) = 9, `amount`, 0) ) AS `Sep`,
			SUM( IF( MONTH(`date`) = 10, `amount`, 0) ) AS `Oct`,
			SUM( IF( MONTH(`date`) = 11, `amount`, 0) ) AS `Nov`,
			SUM( IF( MONTH(`date`) = 12, `amount`, 0) ) AS `Dec`
		'))
		->where(DB::raw('YEAR(`date`)'), '=', date('Y'))
		->first();

		$data['InventoryPurchase'] = InventoryVoucher::select(DB::raw('
			SUM( IF( MONTH(`created_at`) = 1, `total_cost`, 0) ) AS `Jan`,
			SUM( IF( MONTH(`created_at`) = 2, `total_cost`, 0) ) AS `Feb`,
			SUM( IF( MONTH(`created_at`) = 3, `total_cost`, 0) ) AS `Mar`,
			SUM( IF( MONTH(`created_at`) = 4, `total_cost`, 0) ) AS `Apr`,
			SUM( IF( MONTH(`created_at`) = 5, `total_cost`, 0) ) AS `May`,
			SUM( IF( MONTH(`created_at`) = 6, `total_cost`, 0) ) AS `Jun`,
			SUM( IF( MONTH(`created_at`) = 7, `total_cost`, 0) ) AS `Jul`,
			SUM( IF( MONTH(`created_at`) = 8, `total_cost`, 0) ) AS `Aug`,
			SUM( IF( MONTH(`created_at`) = 9, `total_cost`, 0) ) AS `Sep`,
			SUM( IF( MONTH(`created_at`) = 10, `total_cost`, 0) ) AS `Oct`,
			SUM( IF( MONTH(`created_at`) = 11, `total_cost`, 0) ) AS `Nov`,
			SUM( IF( MONTH(`created_at`) = 12, `total_cost`, 0) ) AS `Dec`
		'))
		->where('type', 'receive')
		->where(DB::raw('YEAR(`created_at`)'), '=', date('Y'))
		->first();

		$data['WeeksOfDonation'] = PacketsIssueToCollector::selectRaw(
			'COUNT(*) AS `no_count`, CONCAT(WEEK(`created_at`), " WEEK") AS `week`, SUM(`no_of_packets`) AS `no_of_packets`'
		)
		->where('created_at', '>=', now()->subMonth(1)->toDateString())
		->groupBy('week')
		->orderBy('week')
		->get();

		$data['SumByType'] = Vouchers::selectRaw('sum(`amount`) AS `amount`, `zakat`')
			->where('created_at', '>=', now()->subMonth(1)->toDateString())
			->groupBy('zakat')
			->get();

		$data['CollectorCountCityWise'] = Collector::join('areas', 'collectors.area_id', '=', 'areas.id')
													->join('cities', 'areas.city_id', '=', 'cities.id')
													->selectRaw('count(`collectors`.`id`) AS `no_count`, `cities`.`id`, `cities`.`city_name`')
													->groupBy('cities.id')
													->groupBy('cities.city_name')
													->orderBy('cities.id')
													->get();

		$data['CollectorCountPriorityWise'] = Collector::join('priorities', 'collectors.priority_id', '=', 'priorities.id')
													->selectRaw('count(`collectors`.`id`) AS `no_count`, `priorities`.`id`, `priorities`.`title`')
													->groupBy('priorities.id')
													->groupBy('priorities.title')
													->orderBy('priorities.id')
													->get();

//		dd($data['CollectorCountCityWise']);
//		dd($data['CollectorCountPriorityWise']);

        return view('welcome', $data);
    }

}
