<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;

class UserController extends Controller {

	public function __invoke(){
		return view('user_setting');
	}

	public function PostUpdateInfo(Request $request){

		$request->validate([
			'name' => 'required|min:4',
			'email' => 'required|min:4',
			'password' => 'required|min:4',
		]);

		if(Hash::check($request->password, Auth::user()->password)){

			Auth::user()->fill([
				'name' => $request->name,
				'email' => $request->email
				])->save();

		} else {
			return redirect()->route('Profile')->withErrors('password match error');
		}

		return redirect()->route('Profile')->with(['success'	=>  'Data has been updated']);

	}

	public function PostUpdatePwd(Request $request){
		$request->validate([
			'current_password' => 'required|min:4',
			'new_password' => 'required|confirmed|different:current_password|min:4',
			'new_password_confirmation' => 'required|min:4',
		]);

		if(Hash::check($request->current_password, Auth::user()->password)){
			Auth::user()->fill([
				'password' => Hash::make($request->new_password)
				])->save();

		} else {
			return redirect()->route('Admin.User')->withErrors('password match error');
		}

		return redirect()->route('Admin.User')->with(['success'	=>  'Data has been updated']);
	}


}
