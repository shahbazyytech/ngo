<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Db\Models\Vouchers;


class VouchersController extends Controller
{

	public function __invoke()
	{
		return view('vouchers.vouchers', ['Vouchers' => Vouchers::all()]);
	}

	public function VouchersForm()
	{
		return view('vouchers.vouchers_form');
	}

	public function CreateVouchers(Request $request){
		
		$request->validate([
			'name' => 'required',
			'amount' => 'required',
			'payment_mode' => 'required',
			
		]);
	
		Vouchers::create($request->only(['name', 'amount', 'payment_mode', 'zakat', 'restricted', 'note', 'date']));
		return redirect()->route('Vouchers')->with(['success'	=> 'Data Updated!']);
	}

	public function GetEditVouchers($id){
		return view('vouchers.vouchers_edit', ['Vouchers' => Vouchers::findOrFail($id)]);
	}

	public function PostEditVouchers(Request $request, $id){
	
		$voucher = Vouchers::findOrFail($id);
		$request->validate([
			'name' => 'required',
			'amount' => 'required',
			'payment_mode' => 'required',
		]);

		$voucher->update($request->only(['name', 'amount', 'payment_mode', 'zakat', 'restricted', 'note', 'date']));
		return redirect()->route('Vouchers')->with(['success'	=> 'Data Updated!']);

	}

}
