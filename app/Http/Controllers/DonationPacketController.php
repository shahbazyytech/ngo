<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Db\Models\DonationPacket;
use App\Db\Models\RashanPacket;
use App\Db\Models\Item;
use App\Db\Models\Inventory;
use App\Db\Models\InventoryVoucher;
use App\Db\Models\InventoryVoucherLines;

class DonationPacketController extends Controller
{

	public function __invoke(){
//		dd(DonationPacket::with('RashanPackets')->get());
		return view('donation.packets', ['Packets' => DonationPacket::with('RashanPackets')->get()]);
	}

	public function PacketForm(){

		$item	=	Item::all();

		return view('donation.packets_form', ['Items'=>$item]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$request->validate([
			'per_page' => 'sometimes|integer|max:50',
		]);
		$per_page = $request->has('per_page')? $request->input('per_page') : 15;
		return DonationPacket::paginate($per_page);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function CreatePacket(Request $request)
	{
		$request->validate([
			'name' => 'required',
			'no_of_packets'	=> 'required|integer',
			'cash'	=>	'required_without:rashan',
			'rashan'	=>	'required_without:cash',
		]);

//		dd($request->all());

		$DonationPacket = DonationPacket::create($request->only([
			'name', 'cash', 'no_of_packets', 'unit_cost', 'transportation', 'mazdoori'
		]) + ['available' => $request->input('no_of_packets'), 'status'	=>	'available']);

		foreach($request->input('rashan') as $rashan){
			RashanPacket::create([
				'donation_packet_id'	=>	$DonationPacket->id,
				'item_id'	=>	$rashan['item_id'],
				'name'	=>	$rashan['name'],
				'value'	=>	$rashan['value'],
				'unit'	=>	$rashan['unit'],
				'unit_cost'	=>	$rashan['unit_cost'],
			]);
			$this->FifoConsumeQty($rashan['item_id'], ($rashan['value'] * $request->input('no_of_packets')));
			Item::find($rashan['item_id'])->update(['qty' => $rashan['remain_qty']]);
		}

		return redirect()->route('DonationPackets')->with(['success' => 'Data Updated!']);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function GetEditPacket($id)
	{
		$DonationPacket	=	DonationPacket::findOrFail($id);
		$RashanPackets	=	$DonationPacket->RashanPackets()->with('Item')->get();
//		dd($RashanPackets);
		$RashanPackets	=	$RashanPackets->map(function($packet){
			return [
				'id'	=>	$packet->item->id,
				'avg_cost'	=>	$packet->unit_cost,
				'input_qty'	=>	$packet->value,
				'name'		=>	$packet->item->name,
				'qty'		=>	$packet->item->qty,
				'unit'		=>	$packet->item->unit,
			];
		});
//		dd($RashanPackets);
		if($DonationPacket->available != $DonationPacket->no_of_packets){
			return redirect()->route('DonationPackets')->withErrors(['Packet can not edit','Packets are issued to volunteer']);
		}
		return view('donation.packet_edit', ['DonationPacket'	=>	$DonationPacket, 'RashanPackets'	=>	$RashanPackets, 'Items'	=>	Item::all()]);
	}

	public function GetAddPacket($id)
	{
		$DonationPacket	=	DonationPacket::findOrFail($id);
		return view('donation.packet_add', ['DonationPacket'	=>	$DonationPacket]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function PostEditPacket(Request $request, $id)
	{

//		dd($request->all());

		$DonationPacket =	DonationPacket::with('RashanPackets')->findOrFail($id);
/*
		if($request->has('add_packets')){
			$request->validate([
				'name' => 'required',
				'add_packets'	=> 'required',
			]);

			$DonationPacket->update(['no_of_packets' => ($DonationPacket->no_of_packets + $request->input('add_packets')), 'available' => ($request->input('add_packets') + $DonationPacket->available)]);
			return redirect()->route('DonationPackets')->with(['success' => 'Data Updated!']);
		}
*/
		$request->validate([
			'name' => 'required',
			'no_of_packets'	=> 'required|integer',
			'rashan'	=>	'required_without:cash',
			'cash'	=>	'required_without:rashan'
		]);
		if($DonationPacket->available == $DonationPacket->no_of_packets){

//			$this->RollBackVoucher();

			$InventoryVoucher =	InventoryVoucher::create(['voucher_no'	=>	date('Y').'-'.time(), 'description'	=>	'Rashan Packet Adjectment Qty', 'type'	=>	'receive', 'total_cost'	=>	round(($DonationPacket->unit_cost * $DonationPacket->no_of_packets), 2)]);

			foreach($DonationPacket->RashanPackets as $RashanPackets){

				InventoryVoucherLines::create([ 'item_id'	=> $RashanPackets->item_id, 'qty'	=>	$RashanPackets->value, 'voucher_id'	=>	$InventoryVoucher->id, 'unit_cost'	=>	$RashanPackets->unit_cost]);

				Inventory::create(['item_id'	=> $RashanPackets->item_id, 'qty'	=>	$RashanPackets->value, 'available'	=>	$RashanPackets->value, 'inventory_voucher_id'	=>	$InventoryVoucher->id, 'unit_cost'	=>	$RashanPackets->unit_cost]);

				$item = Item::find($RashanPackets->item_id);
				$item->qty += $RashanPackets->value;
				$item->save();

			}

			$DonationPacket->update($request->only([
				'name', 'no_of_packets', 'unit_cost', 'transportation', 'mazdoori'
			]) + ['available' => $request->input('no_of_packets'), 'status'	=>	'available']);

			RashanPacket::where('donation_packet_id',	$DonationPacket->id)->delete();

			foreach($request->input('rashan') as $rashan){
				RashanPacket::create([
					'donation_packet_id'	=>	$DonationPacket->id,
					'item_id'	=>	$rashan['item_id'],
					'name'	=>	$rashan['name'],
					'value'	=>	$rashan['value'],
					'unit'	=>	$rashan['unit'],
					'unit_cost'	=>	$rashan['unit_cost'],
				]);
				$this->FifoConsumeQty($rashan['item_id'], ($rashan['value'] * $request->input('no_of_packets')));

				$item = Item::find($rashan['item_id']);
				$item->qty -= $rashan['value'];
				$item->save();
			}

			return redirect()->route('DonationPackets')->with(['success' => 'Data Updated!']);
		} else {
			return redirect()->route('DonationPackets')->withErrors(['Packet can not edit','Packets are issued to volunteer']);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id)
	{
		DonationPacket::destroy($id);
		return response()->json(['success' => 'DonationPacket Deleted!'], 201, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
	}

	protected function FifoConsumeQty($item_id, $consume_qty){

			$data= [];

			$inventory = Inventory::where('item_id', $item_id)->Available()->Firstin()->get();

//			dd($inventory);

			foreach ($inventory as $key=>$invent) {

				if($consume_qty >= $invent->available){
					$consume_qty -= $invent->available;
					$invent->available	=	0;
				} else {
					$invent->available	-=	$consume_qty;
					$consume_qty = 0;
				}
				$invent->save();
				if($consume_qty == 0){
					break 1;
				}

			}

	}

	public function CalculateForDonationPacket(Request $request){

		$request->validate([
			'data.*.id'	=> 'required',
			'data.*.input_qty'	=> 'required|numeric|min:1',
		]);

		$data= [];
		foreach($request->input('data') as $k => $value) {

			$inventory = Inventory::where('item_id', $value['id'])->Available()->Firstin()->get();

			$total_costs = [];

			$data[$k] = [
				'id'	=>	 $value['id'],
				'avg_cost'	=>	 0,
			];

//			dd($inventory);

			foreach ($inventory as $key=>$invent) {

				if($invent->available >= $value['input_qty']){
					$total_costs[$key]['cost'] = $invent->unit_cost*$value['input_qty'];
					$total_costs[$key]['qty'] = $value['input_qty'];
				} else {
					$total_costs[$key]['cost'] = $invent->unit_cost*$invent->available;
					$total_costs[$key]['qty'] = $invent->available;
				}

				$collect	= collect($total_costs);
				if($collect->sum('qty') >= $value['input_qty']){
					break 1;
				}

			}

			$tot_qty	=	collect($total_costs)->sum('qty');
			$tot_cost	=	collect($total_costs)->sum('cost');
			
			if($tot_qty){
				$data[$k]['avg_cost'] = round(($tot_cost/$tot_qty), 2);
			} else {
				$data[$k]['avg_cost'] = 0;
			}

		}

		return response()->json(['data' => $data]);
	}


}
