<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Db\Models\Expenses;


class ExpensesController extends Controller
{

	public function __invoke()
	{
		return view('expenses.expenses', ['Expenses' => Expenses::all()]);
	}

	public function ExpensesForm()
	{
		return view('expenses.expenses_form');
	}

	public function CreateExpenses(Request $request){
		
		$request->validate([
			'title' => 'required',
		]);
	
		Expenses::create($request->only(['title']));
		return redirect()->route('Expenses')->with(['success'=> 'Data Updated!']);
	}

	public function GetEditExpenses($id){
		return view('expenses.expenses_edit', ['Expenses' => Expenses::findOrFail($id)]);
	}

	public function PostEditExpenses(Request $request, $id){
	
		$expenses = Expenses::findOrFail($id);
		$request->validate([
			'title' => 'required',
		]);

		$expenses->update($request->only(['title']));
		return redirect()->route('Expenses')->with(['success'	=> 'Data Updated!']);

	}

}
