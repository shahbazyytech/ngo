<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Db\Models\User;
use DB;

class UsersController extends Controller
{

	public function __invoke()
	{
		return view('users.users', ['Users' => User::all()]);
	}

	public function UserForm()
	{
		return view('users.user_form');
	}

	public function CreateUser(Request $request){

		$request->validate([
			'name' => 'required|min:4',
			'role' => 'required',
			'email' => 'required|min:4|unique:users',
			'password' => 'required|confirmed|min:4',
			'password_confirmation' => 'required|min:4',
		]);

		$User	=	User::create($request->only(['name', 'email']) + ['password'	=>	bcrypt($request->input('password')), 'active', $request->input('active')]);
		$User->assignRole($request->input('role'));
		return redirect()->route('Users')->with(['success'=> 'Data Updated!']);
	}

	public function GetEditUser($id){
		$user = User::with('roles')->findOrFail($id);
		return view('users.user_edit', ['User' => $user]);
	}

	public function PostEditUser(Request $request, $id){
	
		$user = User::findOrFail($id);
		if($user->id == 1 ){
			return redirect()->route('Users')->withErrors(['User Not allowed to Edit']);
		}
		$request->validate([
			'name' => 'required|min:4',
			'role'	=>	'required'
		]);

		DB::table('model_has_roles')->where('model_id',$user->id)->delete();

		$user->assignRole($request->input('role'));
		$user->update($request->only(['name']) + ['active' => $request->has('active')]);
		return redirect()->route('Users')->with(['success'	=> 'Data Updated!']);

	}

}
