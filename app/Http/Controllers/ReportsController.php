<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Db\Models\Province;
use App\Db\Models\City;
use App\Db\Models\Area;
use App\Db\Models\Collector;
use App\Db\Models\Volunteer;
use App\Db\Models\Inventory;
use App\Db\Models\Priority;

class ReportsController extends Controller
{

	public function Inventory(Request $request){
		if($request->has('date')){
			$data['Inventory'] = Inventory::with('Item')->Firstin()->Available()->whereBetween('created_at', $request->input('date'))->get();
		} else{
			$data['Inventory']	=	null;
		}
		return view('reports.inventory', $data);
	}

	public function AreaWiseNeedyPerson(Request $request){

		$data = ['Provinces' => Province::all(), 'Cities'	=>	City::all(), 'Areas' => Area::where('parent_id', null)->get()];

		if($request->has('area_id')){
			$data['Priorities']	=	Priority::all();
			$data['Area'] = Area::with(['InnerArea' => function($qry){
				$qry->with('Volunteers');
			}])->findOrFail($request->input('area_id'));
			$data['Needs'] = DB::table('collectors')
			->join('collector_needs', 'collectors.id', '=', 'collector_needs.collector_id')
			->select('collectors.id', 'collectors.area_id', 'collectors.priority_id')
			->whereIn('collectors.area_id', $data['Area']->InnerArea->pluck('id'))
			->where('status', 'pending');
			if($request->has('is_need')){
				$data['Needs']	=	$data['Needs']->where('collector_needs.next_collection_date', '<=', now()->todateString())->get();
			} else {
				$data['Needs']	=	$data['Needs']->get();
			}
		} else {
			$data['Area'] = null;
			$data['Needs'] = null;
			$data['Priorities'] = null;
		}
		
		return view('reports.area_wise_needy_person', $data);
	}


	public function AvailableNeedyPerson(Request $request){

		$data = ['Provinces' => Province::all(), 'Cities'	=>	City::all(), 'Areas' => Area::where('parent_id', null)->get()];

		if($request->has('area_id')){
			$data['Priorities']	=	Priority::all();
			$data['Area'] = Area::with(['InnerArea' => function($qry){
				$qry->with('Volunteers');
			}])->findOrFail($request->input('area_id'));
			$data['Needs'] = DB::table('collectors')
			->join('collector_needs', 'collectors.id', '=', 'collector_needs.collector_id')
			->select('collectors.*', 'collector_needs.priority_id AS need_priority_id')
			->whereIn('collectors.area_id', $data['Area']->InnerArea->pluck('id'))
			->where('status', 'pending');
			if($request->has('is_need')){
				$data['Needs']	=	$data['Needs']->where('collector_needs.next_collection_date', '<=', now()->todateString())->get();
			} else {
				$data['Needs']	=	$data['Needs']->get();
			}
		} else {
			$data['Area'] = null;
			$data['Needs'] = null;
			$data['Priorities'] = null;
		}

		return view('reports.available_needy_person', $data);
	}

	public function AreaWiseVolunteers(Request $request){

		$data = ['Provinces' => Province::all(), 'Cities'	=>	City::all(), 'Areas' => null];

		if($request->has('city_id')){
			$data['Areas'] = Area::with(['InnerArea' => function($qry){
				$qry->with('Volunteers');
			}])->where('city_id', $request->input('city_id'))->get();
		}

		return view('reports.area_wise_volunteers', $data);
	}

	public function VolunteerWiseDistribution(Request $request){
		$data = ['Provinces' => Province::all(), 'Cities'	=>	City::all(), 'Areas' => null];

		if($request->has('city_id')){
			$data['Areas'] = Area::with(['InnerArea' => function($qry){
				$qry->with(['Volunteers'	=>	function($qry){
					return $qry->with(['PacketsIssueToVolunteer'	=>	function($qry){
							return	$qry->where('available', '>', 0);
					}]);
				}]);
			}])->where('city_id', $request->input('city_id'))->get();
		}

		return view('reports.volunteer_wise_distribution', $data);
	}



	public function AreaWise(Request $request){

		$request->validate([
			'province_id' => 'required_without_all:city_id,area_id|exists:provinces,id',
			'city_id' => 'required_without_all:province_id,area_id|exists:cities,id',
			'area_id' => 'required_without_all:city_id,province_id|exists:areas,id',
		]);

		if($request->has('province_id')){

			$data =	Province::with(['Cities'	=>	function($qry){
				return $qry->with(['Areas'	=>	function($qry){
					return $qry->with('Volunteers');
				}]);
			}])->find($request->input('province_id'));

		} else if($request->has('city_id')){

			$data =	City::with(['Areas'	=>	function($qry){
				return $qry->with('Volunteers');
			}])->find($request->input('city_id'));

		} else if($request->has('area_id')){

			$data =	Area::with('Volunteers')->find($request->input('area_id'));

		}

		return response()->json($data, 200, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);
//		return response()->json(['error' => 'request Not completed', 'hint' => 'You can change no_of_packets'], 400, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);

	}

	public function PacketIssueStatus(Request $request){
		$request->validate([
			'province_id' => 'required_without_all:city_id,area_id|exists:provinces,id',
			'city_id' => 'required_without_all:province_id,area_id|exists:cities,id',
			'area_id' => 'required_without_all:city_id,province_id|exists:areas,id',
		]);

		if($request->has('province_id')){

			$data =	Province::with(['Cities'	=>	function($qry){
				return $qry->with(['Areas'	=>	function($qry){
					return $qry->with(['Volunteers'	=>	function($qry){
						return $qry->with(['PacketsIssueToVolunteer' => function($qry){
							return $qry->select('id', 'volunteer_id', 'available', 'no_of_packets')->Available();
						}]);
					}]);
				}]);
			}])->find($request->input('province_id'));

		} else if($request->has('city_id')){

			$data =	City::with(['Areas'	=>	function($qry){
				return $qry->with(['Volunteers'	=>	function($qry){
					return $qry->with(['PacketsIssueToVolunteer' => function($qry){
						return $qry->select('id', 'volunteer_id', 'available', 'no_of_packets')->Available();
					}]);
				}]);
			}])->find($request->input('city_id'));

		} else if($request->has('area_id')){

			$data =	Area::with(['Volunteers'	=>	function($qry){
				return $qry->with(['PacketsIssueToVolunteer' => function($qry){
					return $qry->select('id', 'volunteer_id', 'available', 'no_of_packets')->Available();
				}]);
			}])->find($request->input('area_id'));

		}

		return response()->json($data, 200, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK);

	}


}
