<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Db\Models\Investors;

class InvestorsController extends Controller
{

	public function __invoke()
	{
		return view('investors.investors', ['Investors' => Investors::all()]);
	}

	public function InvestorsForm()
	{
		return view('investors.investors_form');
	}

	public function CreateInvestors(Request $request){
		
		$request->validate([
			'name' => 'required',
			'cnic' => 'required',
			'amount' => 'required',
			'address' => 'required',
			'contact_no' => 'required',
			'payment_mod' => 'required',
		]);
	
		Investors::create($request->only(['name', 'cnic', 'amount', 'payment_mod', 'contact_no', 'address']) + ['future_contact' => $request->has('future_contact')]);
		return redirect()->route('Investors')->with(['success'	=> 'Data Updated!']);
	}

	public function GetEditInvestors($id){
		return view('investors.investors_edit', ['Investor' => Investors::findOrFail($id)]);
	}

	public function PostEditInvestors(Request $request, $id){

		$investors = Investors::findOrFail($id);
		$request->validate([
			'name' => 'required',
			'cnic' => 'required',
			'amount' => 'required',
			'address' => 'required',
			'contact_no' => 'required',
			'payment_mod' => 'required',
		]);
//			dd($request->all());
		$investors->update($request->only(['name', 'cnic', 'amount', 'payment_mod', 'contact_no', 'address']) + ['future_contact' => $request->has('future_contact')]);

		return redirect()->route('Investors')->with(['success'	=> 'Data Updated!']);

	}

}
