<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Db\Models\Area;
use App\Db\Models\Province;
use App\Db\Models\City;

class AreaController extends Controller
{

	public function __invoke()
	{
		return view('area.area',['Areas' => DB::table('areas')
		->join('cities', 'areas.city_id', '=', 'cities.id')
		->join('provinces', 'areas.province_id', '=', 'provinces.id')
		->select('areas.*', 'cities.city_name', 'provinces.name as p_name')
		->get()]);
	}

	public function AreaForm()
	{
		return view('area.area_form', ['Provinces' => Province::all(), 'Cities'	=>	City::all(), 'Areas' => Area::where('parent_id', null)->get()]);
	}

	public function CreateArea(Request $request){
		
		$request->validate([
			'name' => 'required',
			'province_id' => 'required',
			'city_id' => 'required',
		]);
	
		Area::create($request->only(['parent_id','name','province_id','city_id', 'map_lat', 'map_lng']));

		return redirect()->route('Area')->with(['success' => 'Data Updated!']);
	}

	public function GetEditArea($id){
		return view('area.area_edit', ['Area' => Area::findOrFail($id),'Provinces' => Province::all(),'Cities' => City::all(), 'Areas' => Area::where('parent_id', null)->get()]);
	}

	public function PostEditArea(Request $request, $id){

		$Areas = Area::findOrFail($id);
		$request->validate([
			'name' => 'required',
			'province_id' => 'required',
			'city_id' => 'required',
		]);

		$Areas->update($request->only(['parent_id','name','province_id','city_id', 'map_lat', 'map_lng']));
		
		return redirect()->route('Area')->with(['success'	=> 'Data Updated!']);

	}
	public function getcities(Request $request, $id) {

		$data = City::where('province_id', $id)->get();
		return response()->json(array('data'=> $data), 200);
	}

	public function getAreas(Request $request, $id) {

		$data = Area::where('city_id', $id)->get();
		
		return response()->json(array('data'=> $data), 200);
	}


}
