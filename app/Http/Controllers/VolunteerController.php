<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Db\Models\Volunteer;
use App\Db\Models\Area;
use App\Db\Models\Province;
use App\Db\Models\City;


class VolunteerController extends Controller
{

	
	public function __invoke()
	{
		return view('volunteer.volunteer', ['Volunteers' => DB::table('volunteers')
									->join('areas', 'volunteers.area_id', '=', 'areas.id')
									->join('cities', 'areas.city_id', '=', 'cities.id')
									->join('provinces', 'areas.province_id', '=', 'provinces.id')
									->select('volunteers.*','areas.name as area', 'cities.city_name', 'provinces.name as p_name')
									->get()]);
	}
	
	public function VolunteerForm()
	{
		return view('volunteer.volunteer_form', ['Provinces' => Province::all(), 'Cities'	=>	City::all(), 'Areas' => Area::with('InnerArea')->where('parent_id', null)->get()]);
	}

	public function CreateVolunteer(Request $request){
		$request->validate([
			'name' => 'required',
			'cnic' => 'required',
			'address' => 'required',
			'contact_no' => 'required',
			'area_id' => 'required',
			'age' => 'required',
		]);
	
		Volunteer::create($request->only(['name', 'cnic', 'address', 'contact_no', 'skills', 'area_id', 'age']));
		return redirect()->route('Volunteer')->with(['success' => 'Data Updated!']);
	}

	public function GetEditVolunteer($id){
		return view('volunteer.volunteer_edit', ['Provinces' => Province::all(),'Cities' => City::all(), 'Areas' => Area::with('InnerArea')->where('parent_id', null)->get(), 'Volunteer' => Volunteer::with('Area')->findOrFail($id)]);

	}

	public function PostEditVolunteer(Request $request, $id){

		$volunteer = Volunteer::findOrFail($id);
		$request->validate([
			'name' => 'required',
		]);

		$volunteer->update($request->only(['name', 'cnic', 'address', 'contact_no', 'skills', 'area_id', 'age']));
		
		return redirect()->route('Volunteer')->with(['success'	=> 'Data Updated!']);

	}


}
