<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Db\Models\ExpenseVoucher;
use App\Db\Models\Expenses;
use DB;


class ExpenseVouchersController extends Controller
{

	public function __invoke()
	{
		return view('expense_vouchers.expense_vouchers', ['Vouchers' =>  DB::table('expense_vouchers')
		->join('expenses', 'expenses.id', '=', 'expense_vouchers.expense_id')
		->select('expenses.title', 'expense_vouchers.*')
		->get()]);
	}

	public function ExpenseVouchersForm()
	{
		return view('expense_vouchers.expense_vouchers_form', ['Expenses' => Expenses::all()]);
	}

	public function CreateExpenseVouchers(Request $request){
		
		$request->validate([
			'expense_id' => 'required',
			'amount' => 'required',
			'payment_mode' => 'required',
			
		]);
	
		ExpenseVoucher::create($request->only(['expense_id', 'amount', 'payment_mode', 'paid_by','date']));
		return redirect()->route('ExpenseVouchers')->with(['success'	=> 'Data Updated!']);
	}

	public function GetEditExpenseVouchers($id){
		return view('expense_vouchers.expense_vouchers_edit', ['Expenses' => Expenses::all(), 'Vouchers' =>  DB::table('expense_vouchers')
		->select('expenses.title', 'expense_vouchers.*')
		->join('expenses', 'expenses.id', '=', 'expense_vouchers.expense_id')
		->where('expense_vouchers.id',$id)
		->get()]);
	}

	public function PostEditExpenseVouchers(Request $request, $id){
	
		$voucher = ExpenseVoucher::findOrFail($id);
		$request->validate([
			'expense_id' => 'required',
			'amount' => 'required',
			'payment_mode' => 'required',
		]);

		$voucher->update($request->only(['expense_id', 'amount', 'payment_mode', 'paid_by','date']));
		return redirect()->route('ExpenseVouchers')->with(['success' => 'Data Updated!']);

	}

}
