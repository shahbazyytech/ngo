<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Db\Models\Volunteer;
use App\Db\Models\Area;
use App\Db\Models\Province;
use App\Db\Models\City;
use App\Db\Models\Item;
use App\Db\Models\InventoryVoucher;
use App\Db\Models\InventoryVoucherLines;
use App\Db\Models\Inventory;

use DataTables;


class ItemsController extends Controller
{

	
	public function __invoke(Request $request)
	{
/* 		if($request->ajax()){
			return DataTables::eloquent(Item::query())
			->addColumn('actions', '<a href="{{ route(\'GetEditItem\', $id) }}" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>')
			->rawColumns(['actions'])
			->make();
		} */
		return view('items.item', ['Items'	=> Item::all()]);
//		return view('items.item');
	}
	
	public function ItemForm()
	{
		return view('items.item_form');
	}

	public function CreateItem(Request $request){

		$request->validate([
			'name' => 'required',
			'qty' => 'required|integer',
			'rate' => 'required',
			'unit' => 'required',
		]);

		$item	=	Item::create($request->only(['name', 'qty', 'unit', 'rate', 'remarks']));

		$InventoryVoucher =	InventoryVoucher::create(['voucher_no'	=>	date('Y').'-'.time(), 'description'	=>	'Open stock Qty', 'type'	=>	'receive', 'total_cost'	=>	round(($item->qty * $item->rate), 2)]);

		InventoryVoucherLines::create(['item_id'	=> $item['id'], 'qty'	=>	$item['qty'], 'voucher_id'	=>	$InventoryVoucher->id, 'unit_cost'	=>	$item['rate']]);

		Inventory::create(['item_id'	=> $item['id'], 'qty'	=>	$item['qty'], 'inventory_voucher_id'	=>	$InventoryVoucher->id, 'unit_cost'	=>	$item['rate'], 'available'	=>	$item['qty']]);

		return redirect()->route('Items')->with(['success' => 'Data Updated!']);
	}

	public function GetEditItem($id){
		return view('items.item_edit', ['Item' => Item::findOrFail($id)]);
	}

	public function PostEditItem(Request $request, $id){

		$Item = Item::findOrFail($id);
		$request->validate([
			'name' => 'required',
/* 			'qty' => 'required|integer',
			'rate' => 'required', */
			'unit' => 'required',
		]);

		$Item->update($request->only(['name', 'unit', 'remarks']));

		return redirect()->route('Items')->with(['success'	=> 'Data Updated!']);

	}


}
