<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Db\Models\Collector;
use App\Db\Models\CollectorNeed;
use App\Db\Models\Priority;
use App\Db\Models\Area;
use App\Db\Models\Province;
use App\Db\Models\City;

class CollectorController extends Controller
{

	public function __invoke()
	{
		return view('collector.collector', ['Collectors' => Collector::all()]);
	}

	public function CollectorForm()
	{
		return view('collector.collector_form',['Provinces' => Province::all(), 'Cities'	=>	City::all(), 'Areas' => Area::with('InnerArea')->where('parent_id', null)->get(), 'Priorities' => Priority::all()]);
	}

	public function NeedForm($id){
		return view('collector.register_need', ['Priorities' => Priority::all(), 'Collector' => Collector::findOrFail($id)]);
	}

	public function RegisterNeed(Request $request, $id){

		$request->validate([
			'priority_id' => 'required',
			'rashan'	=>	'required_without:cash',
			'cash'	=>	'required_without:rashan'
		]);

		CollectorNeed::create([
			'collector_id'	=>	$id,
			'priority_id'	=>	$request->input('priority_id'),
			'rashan'	=>	$request->has('rashan'),
			'cash'	=>	$request->has('cash'),
			'status'	=>	'pending',
		]);

		return redirect()->route('Collector')->with(['success'	=> 'Data Updated!']);
	}

	public function CreateCollector(Request $request){
		$request->validate([
			'name' => 'required',
			'priority_id' => 'required',
			'cnic' => 'required|unique:collectors,cnic',
			'address' => 'required',
			'contact_no' => 'required',
			'area_id' => 'required',
		]);
	
		$Collector =	Collector::create($request->only(['name', 'cnic', 'other_cnic', 'address', 'family_size', 'contact_no', 'need_cash', 'need_rashan', 'source_of_income', 'priority_id', 'skills', 'zakat','area_id', 'refferd_by']));

		if($request->has('collector_need')){
			CollectorNeed::create([
				'collector_id'	=>	$Collector->id,
				'priority_id'	=>	$request->input('priority_id'),
				'rashan'	=>	$request->input('need_rashan'),
				'cash'	=>	$request->input('need_cash'),
				'status'	=>	'pending',
				]);
		}

		return redirect()->route('Collector')->with(['success'	=> 'Data Updated!']);
	}

	public function GetEditCollector($id){
		return view('collector.collector_edit', ['Collector' => Collector::with('Area')->findOrFail($id), 'Provinces' => Province::all(), 'Cities'	=>	City::all(), 'Areas' => Area::with('InnerArea')->where('parent_id', null)->get(), 'Priorities' => Priority::all()]);
	}

	public function PostEditCollector(Request $request, $id){
//		dd($request->all());

		$collector = Collector::findOrFail($id);
		$request->validate([
			'name' => 'required',
			'priority_id' => 'required',
			'cnic' => 'required|unique:collectors,cnic,'.$id,
			'address' => 'required',
			'contact_no' => 'required',
			'area_id' => 'required',
		]);

		$collector->update($request->only(['name', 'cnic', 'other_cnic', 'address', 'family_size', 'contact_no', 'need_cash', 'need_rashan', 'source_of_income', 'priority_id', 'skills', 'zakat', 'area_id', 'refferd_by']));
		
		return redirect()->route('Collector')->with(['success'	=> 'Data Updated!']);

	}

}
