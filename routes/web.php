<?php

//use Illuminate\Support\Facades\Route;
//use Laravel\Passport\Passport;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', function(){
	return view("login");
})->middleware('guest')->name('login');

Route::post('login', "Auth\LoginController@PostLogin")->name('login');
Route::get('logout', "Auth\LoginController@logout")->name('logout');

Route::get('profile', 'UserController')->name('Profile');
Route::post('profile', 'UserController@PostUpdateInfo')->name('PostUpdateInfo');
Route::post('profile/pwd', 'UserController@PostUpdatePwd')->name('PostUpdatePwd');

Route::middleware('auth')->group(function(){

	Route::get('/', 'DashboardController');

	Route::prefix('supplier')->group(function(){
		Route::get('/', 'SupplierController')->name('Supplier');
		Route::get('create', 'SupplierController@SupplierForm')->name('SupplierForm');
		Route::post('create', 'SupplierController@CreateSupplier')->name('CreateSupplier');
		Route::get('{id}/edit', 'SupplierController@GetEditSupplier')->name('GetEditSupplier');
		Route::post('{id}/edit', 'SupplierController@PostEditSupplier')->name('PostEditSupplier');
	});

	Route::prefix('collector')->group(function(){

		Route::group(['middleware' => 'role:admin|volunteer'], function(){
			Route::get('/', 'CollectorController')->name('Collector');
			Route::get('create', 'CollectorController@CollectorForm')->name('CollectorForm');
			Route::post('create', 'CollectorController@CreateCollector')->name('CreateCollector');
		});

		Route::get('{id}/needs', 'CollectorController@NeedForm')->name('NeedForm');
		Route::post('{id}/needs', 'CollectorController@RegisterNeed')->name('RegisterNeed');
		Route::get('{id}/edit', 'CollectorController@GetEditCollector')->name('GetEditCollector');
		Route::post('{id}/edit', 'CollectorController@PostEditCollector')->name('PostEditCollector');
	});

	Route::group(['middleware' => 'role:admin'], function(){

		Route::prefix('user')->group(function(){
			Route::get('/', 'UsersController')->name('Users');
			Route::get('create', 'UsersController@UserForm')->name('UserForm');
			Route::post('create', 'UsersController@CreateUser')->name('CreateUser');
			Route::get('{id}/edit', 'UsersController@GetEditUser')->name('GetEditUser');
			Route::post('{id}/edit', 'UsersController@PostEditUser')->name('PostEditUser');
		});

		Route::prefix('Area')->group(function(){
			Route::get('/', 'AreaController')->name('Area');
			Route::get('create', 'AreaController@AreaForm')->name('AreaForm');
			Route::post('create', 'AreaController@CreateArea')->name('CreateArea');
			Route::get('{id}/edit', 'AreaController@GetEditArea')->name('GetEditArea');
			Route::post('{id}/edit', 'AreaController@PostEditArea')->name('PostEditArea');
		/*     Route::get('{id}/getcities', 'AreaController@getcities')->name('getcities'); */
		/*     Route::get('{id}/getAreas', 'AreaController@getAreas')->name('getAreas'); */
		});

		Route::prefix('Volunteer')->group(function(){
			Route::get('/', 'VolunteerController')->name('Volunteer');
			Route::get('create', 'VolunteerController@VolunteerForm')->name('VolunteerForm');
			Route::post('create', 'VolunteerController@CreateVolunteer')->name('CreateVolunteer');
			Route::get('{id}/edit', 'VolunteerController@GetEditVolunteer')->name('GetEditVolunteer');
			Route::post('{id}/edit', 'VolunteerController@PostEditVolunteer')->name('PostEditVolunteer');
		});

		Route::prefix('donation-packets')->group(function(){
			Route::get('/', 'DonationPacketController')->name('DonationPackets');
			Route::get('create', 'DonationPacketController@PacketForm')->name('DonationPacketForm');
			Route::post('create', 'DonationPacketController@CreatePacket')->name('CreatePacket');
			Route::get('calculate', 'DonationPacketController@CalculateForDonationPacket')->name('CalculateForDonationPacket');
			Route::get('{id}/edit', 'DonationPacketController@GetEditPacket')->name('GetEditPacket');
			Route::post('{id}/edit', 'DonationPacketController@PostEditPacket')->name('PostEditPacket');
			Route::get('{id}/add-packets', 'DonationPacketController@GetAddPacket')->name('GetAddPacket');
		});

	});

	Route::group(['middleware'	=>	'role:admin|finance'], function(){

		Route::prefix('items')->group(function(){
			Route::get('/', 'ItemsController')->name('Items');
			Route::get('create', 'ItemsController@ItemForm')->name('ItemForm');
			Route::post('create', 'ItemsController@CreateItem')->name('CreateItem');
			Route::get('{id}/edit', 'ItemsController@GetEditItem')->name('GetEditItem');
			Route::post('{id}/edit', 'ItemsController@PostEditItem')->name('PostEditItem');
		});

		Route::prefix('inventory-vouchers')->group(function(){
			Route::get('/', 'InventoryVouchersController')->name('InventoryVouchers');
			Route::get('create', 'InventoryVouchersController@InventoryVoucherForm')->name('InventoryVoucherForm');
			Route::post('create', 'InventoryVouchersController@CreateInventoryVoucher')->name('CreateInventoryVoucher');
			Route::get('{id}/edit', 'InventoryVouchersController@GetEditInventoryVoucher')->name('GetEditInventoryVoucher');
			Route::post('{id}/edit', 'InventoryVouchersController@PostEditInventoryVoucher')->name('PostEditInventoryVoucher');
		});

		Route::prefix('Investors')->group(function(){
			Route::get('/', 'InvestorsController')->name('Investors');
			Route::get('create', 'InvestorsController@InvestorsForm')->name('InvestorsForm');
			Route::post('create', 'InvestorsController@CreateInvestors')->name('CreateInvestors');
			Route::get('{id}/edit', 'InvestorsController@GetEditInvestors')->name('GetEditInvestors');
			Route::post('{id}/edit', 'InvestorsController@PostEditInvestors')->name('PostEditInvestors');
		});

		Route::prefix('Vouchers')->group(function(){
			Route::get('/', 'VouchersController')->name('Vouchers');
			Route::get('create', 'VouchersController@VouchersForm')->name('VouchersForm');
			Route::post('create', 'VouchersController@CreateVouchers')->name('CreateVouchers');
			Route::get('{id}/edit', 'VouchersController@GetEditVouchers')->name('GetEditVouchers');
			Route::post('{id}/edit', 'VouchersController@PostEditVouchers')->name('PostEditVouchers');
		});

		Route::prefix('reports')->group(function(){
			Route::get('area-wise-needy-person', 'ReportsController@AreaWiseNeedyPerson')->name('AreaWiseNeedyPerson');
			Route::get('available-needy-person', 'ReportsController@AvailableNeedyPerson')->name('AvailableNeedyPerson');
			Route::get('area-wise-volunteers', 'ReportsController@AreaWiseVolunteers')->name('AreaWiseVolunteers');
			Route::get('volunteer-wise-distribution', 'ReportsController@VolunteerWiseDistribution')->name('VolunteerWiseDistribution');
			Route::get('inventory', 'ReportsController@Inventory')->name('InventoryReport');
		});

		Route::prefix('expense-type')->group(function(){
			Route::get('/', 'ExpensesController')->name('Expenses');
			Route::get('create', 'ExpensesController@ExpensesForm')->name('ExpensesForm');
			Route::post('create', 'ExpensesController@CreateExpenses')->name('CreateExpenses');
			Route::get('{id}/edit', 'ExpensesController@GetEditExpenses')->name('GetEditExpenses');
			Route::post('{id}/edit', 'ExpensesController@PostEditExpenses')->name('PostEditExpenses');
		});
		Route::prefix('expense-vouchers')->group(function(){
			Route::get('/', 'ExpenseVouchersController')->name('ExpenseVouchers');
			Route::get('create', 'ExpenseVouchersController@ExpenseVouchersForm')->name('ExpenseVouchersForm');
			Route::post('create', 'ExpenseVouchersController@CreateExpenseVouchers')->name('CreateExpenseVouchers');
			Route::get('{id}/edit', 'ExpenseVouchersController@GetEditExpenseVouchers')->name('GetEditExpenseVouchers');
			Route::post('{id}/edit', 'ExpenseVouchersController@PostEditExpenseVouchers')->name('PostEditExpenseVouchers');
		});

	});

	Route::prefix('packet-issue')->group(function(){

		Route::group(['middleware' => 'role:admin'], function(){
			Route::get('volunteer', 'PacketsIssueController@GetPacketsIssueVolunteer')->name('GetPacketsIssueVolunteer');
			Route::post('volunteer', 'PacketsIssueController@PostPacketsIssueVolunteer')->name('PostPacketsIssueVolunteer');
		});
		Route::get('get-data', 'PacketsIssueController@GetPacketsIssueCollectorData')->name('GetPacketsIssueCollectorData');

		Route::group(['middleware' => 'role:admin|volunteer'], function(){
			Route::get('collector', 'PacketsIssueController@GetPacketsIssueCollector')->name('GetPacketsIssueCollector');
			Route::post('collector', 'PacketsIssueController@PostPacketsIssueCollector')->name('PostPacketsIssueCollector');
		});


	});


	//Auth::routes();
	//Passport::routes();

});

