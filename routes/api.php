<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

Route::group(['namespace' => 'Api\V1'], function(){
/* 
	Route::resource('donation-packets', 'DonationPacketController')->except([
		'create', 'edit', 'destroy'
	]);
	Route::resource('collector-needs', 'CollectorNeedController')->except([
		'create', 'edit', 'destroy'
	]);

	Route::post('packet-issue-to-volunteer', 'PacketsIssueController@PacketIssueToVolunteer');
	Route::post('packets-issue-to-volunteer', 'PacketsIssueController@PacketsIssueToVolunteer');

	Route::post('packet-issue-to-collector', 'PacketsIssueController@PacketIssueToCollector');
	Route::post('packets-issue-to-collector', 'PacketsIssueController@PacketsIssueToCollector');

	Route::prefix('reports')->group(function(){
		Route::post('volunteers-area-wise', 'ReportsController@AreaWise');
		Route::post('volunteers-packet-issue-status', 'ReportsController@PacketIssueStatus');
	});
 */
});

