-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 11, 2020 at 12:36 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ngo`
--

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE `areas` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `province_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `map_lat` float DEFAULT NULL,
  `map_lng` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`id`, `parent_id`, `name`, `province_id`, `city_id`, `map_lat`, `map_lng`, `created_at`, `updated_at`) VALUES
(1, NULL, 'gulistan e johar', 1, 1, 0.3, 0.3, NULL, NULL),
(2, NULL, 'Saddar', 1, 1, 0.4, 0.5, NULL, NULL),
(3, NULL, 'DHA', 2, 117, 0.34, 0.56, '2020-04-08 16:01:47', '2020-04-08 16:51:25'),
(4, 1, 'Block 1', 1, 1, 23, 23, '2020-04-14 04:26:51', '2020-04-14 04:26:51'),
(5, NULL, 'Shah Faisal Colony', 1, 1, 25, 25, '2020-04-15 01:50:57', '2020-04-15 01:50:57'),
(6, NULL, 'Shah Faisal Colonyb 1', 1, 1, 5, 5, '2020-04-15 01:53:46', '2020-04-15 01:53:46'),
(7, NULL, 'Shah Faisal Colonyb 1asdfa', 1, 1, 5, 5, '2020-04-15 01:59:16', '2020-04-15 01:59:16');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `province_id`, `city_name`) VALUES
(1, 1, 'Karachi'),
(2, 1, 'Hyderabad'),
(3, 3, 'Quetta'),
(4, 3, 'Sibi'),
(5, 4, 'Peshawar'),
(6, 4, 'Charsadda'),
(7, 2, 'Lahore'),
(8, 2, 'Rawalpindi'),
(9, 2, 'Faisalabad'),
(10, 2, 'Islamabad'),
(11, 1, 'Badin'),
(12, 1, 'Bhirkan'),
(13, 1, 'Bhiria City'),
(14, 1, 'Bhiria Road'),
(15, 1, 'Rajo Khanani'),
(16, 1, 'Chak'),
(17, 1, 'Dadu'),
(18, 1, 'Digri'),
(19, 1, 'Diplo'),
(20, 1, 'Dokri'),
(21, 1, 'Gambat'),
(22, 1, 'Garhi Yasin'),
(23, 1, 'Ghotki'),
(24, 1, 'Islamkot'),
(25, 1, 'Jacobabad'),
(26, 1, 'Jamshoro'),
(27, 1, 'Jungshahi'),
(28, 1, 'Kandhkot'),
(29, 1, 'Kandiaro'),
(30, 1, 'Kashmore'),
(31, 1, 'Keti Bandar'),
(32, 1, 'Khadro'),
(33, 1, 'Khairpur'),
(34, 1, 'Khipro'),
(35, 1, 'Kotri'),
(36, 1, 'Larkana'),
(37, 1, 'Madeji'),
(38, 1, 'Matiari'),
(39, 1, 'Mehar'),
(40, 1, 'Mirpur Khas'),
(41, 1, 'Mithani'),
(42, 1, 'Mithi'),
(43, 1, 'Mehrabpur'),
(44, 1, 'Moro'),
(45, 1, 'Nagarparkar'),
(46, 1, 'Naudero'),
(47, 1, 'Naushahro Feroze'),
(48, 1, 'Naushara'),
(49, 1, 'Nawabshah'),
(50, 1, 'Qambar'),
(51, 1, 'Qasimabad'),
(52, 1, 'Ranipur'),
(53, 1, 'Ratodero'),
(54, 1, 'Rohri'),
(55, 1, 'Sakrand'),
(56, 1, 'Sanghar'),
(57, 1, 'Shahbandar'),
(58, 1, 'Shahdadkot'),
(59, 1, 'Shahdadpur'),
(60, 1, 'Shahpur Chakar'),
(61, 1, 'Shikarpaur'),
(62, 1, 'Sinjhoro'),
(63, 1, 'Sukkur'),
(64, 1, 'Tangwani'),
(65, 1, 'Tando Adam Khan'),
(66, 1, 'Tando Allahyar'),
(67, 1, 'Tando Muhammad Khan'),
(68, 1, 'Thatta'),
(69, 1, 'Thari Mirwah'),
(70, 1, 'Umerkot'),
(71, 1, 'Warah'),
(72, 1, 'Piryaloi'),
(73, 1, 'Tharushah'),
(74, 1, 'Sita Road'),
(75, 1, 'Pir Jo Goth'),
(76, 1, 'Shahpur jahania'),
(77, 1, 'Qubo Saeed Khan'),
(78, 1, 'Mehrabpur'),
(79, 1, 'Samaro'),
(80, 1, 'Maqsoodo Rind'),
(81, 1, 'Sann'),
(82, 1, 'Manjhand'),
(83, 1, 'Lakhi ghulam shah'),
(84, 1, 'Mian Sahib'),
(85, 1, 'Khairpur Nathan Shah'),
(86, 1, 'Boriri'),
(87, 2, 'Multan'),
(88, 2, 'Gujranwala'),
(89, 2, 'Sialkot'),
(90, 2, 'Bahawalpur'),
(91, 2, 'Sargodha'),
(92, 2, 'Sahiwal'),
(93, 2, 'Kasur'),
(94, 2, 'Gujrat'),
(95, 2, 'Sheikhupura'),
(96, 2, 'Jhang'),
(97, 2, 'Rahim Yar Khan'),
(98, 2, 'Dera Ghazi Khan'),
(99, 2, 'Chiniot'),
(100, 2, 'Chakwal'),
(101, 2, 'Jhelum'),
(102, 2, 'Okara'),
(103, 2, 'Attock'),
(104, 2, 'Burewala'),
(105, 2, 'Sadiqabad'),
(106, 2, 'Mianwali'),
(107, 2, 'Toba Tek Singh'),
(108, 2, 'Taxila'),
(109, 2, 'Gujar Khan'),
(110, 2, 'Pakpattan'),
(111, 2, 'Wazirabad'),
(112, 2, 'Gojra'),
(113, 2, 'Khanpur'),
(114, 2, 'Vehari'),
(115, 2, 'Bahawalnagar'),
(116, 2, 'Khanewal'),
(117, 2, 'Hafizabad'),
(118, 3, 'Khuzdar'),
(119, 3, 'Turbat'),
(120, 3, 'Chaman'),
(121, 3, 'Hub'),
(122, 3, 'Zhob'),
(123, 3, 'Gwadar'),
(124, 3, 'Dera Murad Jamali'),
(125, 3, 'Dera Allah Yar'),
(126, 3, 'Loralai'),
(127, 3, 'Pasni'),
(128, 3, 'Kharan'),
(129, 3, 'Mastung'),
(130, 3, 'Nushki'),
(131, 3, 'Kalat'),
(132, 4, 'Mardan'),
(133, 4, 'Mingora'),
(134, 4, 'Kohat'),
(135, 4, 'Abbottabad'),
(136, 4, 'Dera Ismail Khan'),
(137, 4, 'Nowshera'),
(138, 4, 'Mansehra'),
(139, 5, 'Mirpur'),
(140, 5, 'Muzaffarabad'),
(141, 5, 'Poonch');

-- --------------------------------------------------------

--
-- Table structure for table `collectors`
--

CREATE TABLE `collectors` (
  `id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `cnic` varchar(64) NOT NULL,
  `other_cnic` varchar(4096) DEFAULT NULL,
  `address` varchar(4096) NOT NULL,
  `family_size` int(11) DEFAULT NULL,
  `contact_no` varchar(64) NOT NULL,
  `need_cash` tinyint(1) DEFAULT NULL,
  `zakat` tinyint(1) DEFAULT NULL,
  `source_of_income` varchar(24) DEFAULT NULL,
  `skills` varchar(2048) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `priority_id` int(11) NOT NULL,
  `need_rashan` tinyint(1) NOT NULL,
  `refferd_by` varchar(2048) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `collectors`
--

INSERT INTO `collectors` (`id`, `name`, `cnic`, `other_cnic`, `address`, `family_size`, `contact_no`, `need_cash`, `zakat`, `source_of_income`, `skills`, `area_id`, `priority_id`, `need_rashan`, `refferd_by`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Bhawish Dass Kanjwani 2', '4550217168318', '4550217168314', 'H.No B-45/1 Rizwani muhallah Rohri District Sukkur update', 2, '03327131372', 1, 1, 'shop update', 'Skills update', 4, 2, 1, 'Ahmer', NULL, '2020-04-07 00:00:00', '2020-04-15 12:00:10'),
(2, 'sdfrrds', '345', '345345', '6grfgtdft', NULL, '1231531', 1, 1, NULL, 'safr', 1, 1, 1, NULL, NULL, '2020-04-09 10:58:15', '2020-04-09 10:58:15'),
(3, 'sdfrrds', '345', '345345', '6grfgtdft', NULL, '1231531', 1, 1, NULL, 'safr', 1, 1, 1, NULL, NULL, '2020-04-09 11:04:23', '2020-04-09 11:04:23'),
(4, 'sdfrrds', '345', '345345', '6grfgtdft', NULL, '1231531', 1, 1, NULL, 'safr', 4, 1, 1, NULL, NULL, '2020-04-09 11:04:51', '2020-04-16 14:24:34');

-- --------------------------------------------------------

--
-- Table structure for table `collector_needs`
--

CREATE TABLE `collector_needs` (
  `id` int(11) NOT NULL,
  `collector_id` int(11) NOT NULL,
  `rashan` tinyint(1) DEFAULT NULL,
  `cash` int(11) DEFAULT NULL,
  `priority_id` int(11) DEFAULT NULL,
  `priority` enum('high','normal','low') DEFAULT NULL,
  `status` enum('pending','completed') NOT NULL,
  `next_collection_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `collector_needs`
--

INSERT INTO `collector_needs` (`id`, `collector_id`, `rashan`, `cash`, `priority_id`, `priority`, `status`, `next_collection_date`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 1, 1, NULL, 'pending', '2020-05-04', NULL, '2020-04-09 11:04:51', '2020-04-09 11:04:51'),
(4, 1, 1, 0, 1, NULL, 'completed', '2020-05-05', NULL, '2020-04-16 14:02:50', '2020-04-21 09:35:00'),
(5, 1, 1, 1, 1, NULL, 'pending', '2020-05-05', NULL, '2020-04-21 03:39:18', '2020-04-21 10:28:56');

-- --------------------------------------------------------

--
-- Table structure for table `donation_packets`
--

CREATE TABLE `donation_packets` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `cash` int(11) DEFAULT NULL,
  `rashan` text,
  `status` enum('available','completed') NOT NULL,
  `no_of_packets` int(11) NOT NULL,
  `available` int(11) DEFAULT NULL,
  `unit_cost` decimal(10,3) NOT NULL,
  `mazdoori` int(11) DEFAULT NULL,
  `transportation` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donation_packets`
--

INSERT INTO `donation_packets` (`id`, `name`, `cash`, `rashan`, `status`, `no_of_packets`, `available`, `unit_cost`, `mazdoori`, `transportation`, `created_at`, `updated_at`, `created_by`) VALUES
(1, 'packet1', NULL, NULL, 'available', 1, 1, '1550.000', 100, 100, '2020-06-10 07:27:48', '2020-06-11 05:36:16', NULL),
(2, 'packet2', NULL, NULL, 'available', 4, 4, '160.000', 100, 100, '2020-06-10 07:37:02', '2020-06-10 07:37:02', NULL),
(3, 'sdf', NULL, NULL, 'available', 2, 2, '150.000', 20, 50, '2020-06-11 05:22:42', '2020-06-11 05:22:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Maintance update', '2020-04-23 02:58:56', '2020-04-23 03:02:32');

-- --------------------------------------------------------

--
-- Table structure for table `expense_vouchers`
--

CREATE TABLE `expense_vouchers` (
  `id` int(11) NOT NULL,
  `expense_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `payment_mode` enum('bank','cash') NOT NULL,
  `paid_by` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `expense_vouchers`
--

INSERT INTO `expense_vouchers` (`id`, `expense_id`, `amount`, `payment_mode`, `paid_by`, `date`, `created_at`, `updated_at`) VALUES
(1, 1, '6003.00', 'bank', 'Barkha update', '2020-04-22', '2020-04-23 14:07:18', '2020-04-23 04:35:53');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE `inventories` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `inventory_voucher_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `unit_cost` decimal(10,2) NOT NULL,
  `available` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `item_id`, `inventory_voucher_id`, `qty`, `unit_cost`, `available`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, '100.00', 2, '2020-06-10 06:58:52', '2020-06-10 06:58:52'),
(2, 2, 2, 2, '120.00', 1, '2020-06-10 06:59:34', '2020-06-10 07:37:02'),
(3, 3, 3, 2, '120.00', 2, '2020-06-10 07:00:42', '2020-06-10 07:00:42'),
(4, 4, 4, 5, '60.00', 5, '2020-06-10 07:03:46', '2020-06-10 07:03:46'),
(5, 5, 5, 20, '150.00', 5, '2020-06-10 07:04:34', '2020-06-11 05:36:16'),
(6, 2, 6, 2, '200.00', 2, '2020-06-10 07:15:34', '2020-06-10 07:15:34'),
(9, 5, 7, 5, '120.00', 5, '2020-06-10 07:16:14', '2020-06-10 07:16:55'),
(10, 4, 8, 5, '50.00', 5, '2020-06-10 07:18:15', '2020-06-10 07:18:15'),
(11, 5, 9, 2, '100.00', 2, '2020-06-10 07:18:45', '2020-06-10 07:18:45'),
(12, 5, 10, 7, '150.00', 7, '2020-06-11 05:36:16', '2020-06-11 05:36:16');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_vouchers`
--

CREATE TABLE `inventory_vouchers` (
  `id` int(11) NOT NULL,
  `voucher_no` varchar(64) NOT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `type` enum('receive','donate') NOT NULL,
  `total_cost` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory_vouchers`
--

INSERT INTO `inventory_vouchers` (`id`, `voucher_no`, `description`, `type`, `total_cost`, `created_at`, `updated_at`, `created_by`) VALUES
(1, '2020-1591790332', 'Open stock Qty', 'receive', 200, '2020-06-10 06:58:52', '2020-06-10 06:58:52', NULL),
(2, '2020-1591790374', 'Open stock Qty', 'receive', 240, '2020-06-10 06:59:34', '2020-06-10 06:59:34', NULL),
(3, '2020-1591790442', 'Open stock Qty', 'receive', 240, '2020-06-10 07:00:42', '2020-06-10 07:00:42', NULL),
(4, '2020-1591790626', 'Open stock Qty', 'receive', 300, '2020-06-10 07:03:46', '2020-06-10 07:03:46', NULL),
(5, '2020-1591790674', 'Open stock Qty', 'receive', 3000, '2020-06-10 07:04:34', '2020-06-10 07:04:34', NULL),
(6, '2020-1591791296', NULL, 'receive', 400, '2020-06-10 07:15:34', '2020-06-10 07:15:34', NULL),
(7, '2020-1591791346', 'Description', 'receive', 600, '2020-06-10 07:16:14', '2020-06-10 07:16:55', NULL),
(8, '2020-1591791423', 'Description', 'receive', 250, '2020-06-10 07:18:15', '2020-06-10 07:18:15', NULL),
(9, '2020-1591791503', NULL, 'receive', 200, '2020-06-10 07:18:45', '2020-06-10 07:18:45', NULL),
(10, '2020-1591871776', 'Rashan Packet Adjectment Qty', 'receive', 1050, '2020-06-11 05:36:16', '2020-06-11 05:36:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inventory_voucher_lines`
--

CREATE TABLE `inventory_voucher_lines` (
  `id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `unit_cost` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory_voucher_lines`
--

INSERT INTO `inventory_voucher_lines` (`id`, `voucher_id`, `item_id`, `qty`, `unit_cost`) VALUES
(1, 1, 1, 2, 100),
(2, 2, 2, 2, 120),
(3, 3, 3, 2, 120),
(4, 4, 4, 5, 60),
(5, 5, 5, 20, 150),
(6, 6, 2, 2, 200),
(9, 7, 5, 5, 120),
(10, 8, 4, 5, 50),
(11, 9, 5, 2, 100),
(12, 10, 5, 7, 150);

-- --------------------------------------------------------

--
-- Table structure for table `investors`
--

CREATE TABLE `investors` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `cnic` varchar(64) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `payment_mod` varchar(12) DEFAULT NULL,
  `future_contact` tinyint(1) DEFAULT NULL,
  `contact_no` varchar(24) DEFAULT NULL,
  `address` varchar(4096) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `investors`
--

INSERT INTO `investors` (`id`, `name`, `cnic`, `amount`, `payment_mod`, `future_contact`, `contact_no`, `address`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'adf-', '3430', 4350, 'bank', 1, '430', 'dfg -', NULL, '2020-04-14 06:29:30', '2020-04-14 11:45:25'),
(2, 'cxs', '34', 243, 'cash', 1, '23', 'sdg', NULL, '2020-04-14 11:46:00', '2020-04-14 11:46:00');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `remarks` varchar(1024) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `unit` varchar(12) NOT NULL,
  `rate` decimal(10,3) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `remarks`, `qty`, `unit`, `rate`, `created_at`, `updated_at`, `created_by`) VALUES
(1, 'rice', NULL, 2, 'kgs', '100.000', '2020-06-10 06:58:52', '2020-06-10 06:58:52', NULL),
(2, 'sugar', NULL, 0, 'kgs', '120.000', '2020-06-10 06:59:34', '2020-06-10 07:37:02', NULL),
(3, 'tea pack', NULL, 2, 'kg', '120.000', '2020-06-10 07:00:42', '2020-06-10 07:01:24', NULL),
(4, 'flour', NULL, 10, 'KG', '60.000', '2020-06-10 07:03:46', '2020-06-10 07:18:15', NULL),
(5, 'oil', 'ok', 25, 'piece', '150.000', '2020-06-10 07:04:34', '2020-06-11 05:36:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(5, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(6, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(7, '2016_06_01_000004_create_oauth_clients_table', 2),
(8, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(9, '2020_05_05_101522_create_permission_tables', 3);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Db\\Models\\User', 1),
(3, 'App\\Db\\Models\\User', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'gNFoY3I4AGEj1FU032FkfgaGlR62XmBapq2026lK', 'http://localhost', 1, 0, 0, '2020-04-03 06:52:13', '2020-04-03 06:52:13'),
(2, NULL, 'Laravel Password Grant Client', 'pIVqE0DcRftLnWQLg23lOkjMTyPz91EZLoIVA7gK', 'http://localhost', 0, 1, 0, '2020-04-03 06:52:13', '2020-04-03 06:52:13');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-04-03 06:52:13', '2020-04-03 06:52:13');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `packets_issue_to_collectors`
--

CREATE TABLE `packets_issue_to_collectors` (
  `id` int(11) NOT NULL,
  `collector_id` int(11) NOT NULL,
  `no_of_packets` int(11) NOT NULL,
  `packets_issue_to_volunteer_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `collector_need_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `packets_issue_to_volunteers`
--

CREATE TABLE `packets_issue_to_volunteers` (
  `id` int(11) NOT NULL,
  `volunteer_id` int(11) NOT NULL,
  `no_of_packets` int(11) NOT NULL,
  `donation_packet_id` int(11) NOT NULL,
  `available` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('muhammadaliecho@gmail.com', '$2y$10$uzuFUo/EUhMyBUaWHF80ku51utuA0V5qgKA.OfUDOALGc33OtZEIS', '2020-04-01 04:30:32');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `priorities`
--

CREATE TABLE `priorities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `color` varchar(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `priorities`
--

INSERT INTO `priorities` (`id`, `name`, `title`, `color`) VALUES
(1, 'high', 'HIGH', 'red'),
(2, 'normal', 'NORMAL', 'yellow');

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE `provinces` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`id`, `name`) VALUES
(1, 'Sindh'),
(2, 'Punjab'),
(3, 'Balochistan'),
(4, 'NWFP'),
(5, 'Azad Kashmir'),
(6, 'FATA'),
(7, 'FANA');

-- --------------------------------------------------------

--
-- Table structure for table `rashan_packets`
--

CREATE TABLE `rashan_packets` (
  `id` int(11) NOT NULL,
  `donation_packet_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `value` varchar(64) NOT NULL,
  `unit` varchar(12) NOT NULL,
  `unit_cost` decimal(10,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rashan_packets`
--

INSERT INTO `rashan_packets` (`id`, `donation_packet_id`, `item_id`, `name`, `value`, `unit`, `unit_cost`) VALUES
(2, 2, 2, 'sugar', '1', 'kgs', '160.000'),
(3, 3, 5, 'oil', '1', 'piece', '150.000'),
(4, 1, 5, 'oil', '7', 'piece', '150.000');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', NULL, NULL),
(2, 'finance', 'web', NULL, NULL),
(3, 'volunteer', 'web', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sample_packets`
--

CREATE TABLE `sample_packets` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `rashan` text,
  `no_of_packets` int(11) DEFAULT NULL,
  `mazdoori` int(11) DEFAULT NULL,
  `transportation` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(2048) NOT NULL,
  `qty` varchar(512) DEFAULT NULL,
  `type_of_rashan` text,
  `daily_supply_limit` varchar(512) DEFAULT NULL,
  `timing` varchar(64) DEFAULT NULL,
  `contact_no` varchar(64) DEFAULT NULL,
  `address` varchar(4096) DEFAULT NULL,
  `map_lat` varchar(24) DEFAULT NULL,
  `map_lng` varchar(24) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `user_id`, `name`, `qty`, `type_of_rashan`, `daily_supply_limit`, `timing`, `contact_no`, `address`, `map_lat`, `map_lng`, `created_by`, `created_at`, `updated_at`) VALUES
(1, NULL, 'name', 'qty of ration', NULL, 'limit 345', 'time 45', '0313', 'full address', '45.258', '132.5662', NULL, '2020-04-01 08:56:16', '2020-04-01 08:56:16'),
(2, NULL, 'name', 'qty of ration', NULL, 'limit 345', 'time 45', '0313', 'full address', '45.258', '132.5662', NULL, '2020-04-01 08:56:37', '2020-04-01 08:56:37'),
(3, NULL, 'ss', 's', 's', 's', 's', 's', 's', 's', 's', NULL, '2020-04-01 09:06:35', '2020-04-01 10:35:15'),
(4, NULL, 's', 's', 's', 's', 's', 's', 's', 's', 's', NULL, '2020-04-01 09:06:43', '2020-04-01 09:06:43'),
(5, NULL, 'df2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-04-14 11:51:21', '2020-04-14 11:51:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_volunteer` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `is_volunteer`, `active`, `created_at`, `updated_at`) VALUES
(1, 'admin1', 'm.ali@yytechnologies.com', NULL, '$2y$10$lr44vorr6pjAP8kr58TameES8AGg/tyMWn6noWiRUKV5EFGVQpcSW', NULL, 0, 1, '2020-04-01 04:30:04', '2020-04-28 06:29:29'),
(2, 'adminq', 'a@a.com', NULL, '$2y$10$0Z6fKwlwAiIfTu5bbJVdN.dJn4MHfgB/F7BRZDyB5ltfN67T/tklK', NULL, 0, NULL, '2020-05-05 15:54:26', '2020-05-05 17:00:49');

-- --------------------------------------------------------

--
-- Table structure for table `volunteers`
--

CREATE TABLE `volunteers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `cnic` varchar(24) NOT NULL,
  `address` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `contact_no` varchar(14) NOT NULL,
  `skills` varchar(64) DEFAULT NULL,
  `area_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `volunteers`
--

INSERT INTO `volunteers` (`id`, `name`, `cnic`, `address`, `age`, `contact_no`, `skills`, `area_id`, `created_at`, `updated_at`, `created_by`) VALUES
(1, 'Bhawish Dass', '455021716831', 'H.No B-45/1 Rizwani muhallah Rohri District', 24, '03327131373', 'Skills update', 4, '2020-04-09 03:28:19', '2020-04-14 06:05:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE `vouchers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `payment_mode` varchar(255) NOT NULL,
  `zakat` tinyint(1) NOT NULL DEFAULT '0',
  `restricted` tinyint(1) NOT NULL DEFAULT '0',
  `note` varchar(600) DEFAULT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vouchers`
--

INSERT INTO `vouchers` (`id`, `name`, `amount`, `payment_mode`, `zakat`, `restricted`, `note`, `date`, `created_at`, `updated_at`) VALUES
(1, 'Barkha update', 60001, 'bank', 1, 1, 'Restrictions Note update', '2020-04-11', '2020-04-09 22:15:53', '2020-04-09 22:28:50'),
(2, 'Barkha update', 60001, 'bank', 0, 1, 'Restrictions Note update', '2020-04-11', '2020-04-09 22:15:53', '2020-04-09 22:28:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collectors`
--
ALTER TABLE `collectors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collector_needs`
--
ALTER TABLE `collector_needs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donation_packets`
--
ALTER TABLE `donation_packets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense_vouchers`
--
ALTER TABLE `expense_vouchers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventories`
--
ALTER TABLE `inventories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_vouchers`
--
ALTER TABLE `inventory_vouchers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_voucher_lines`
--
ALTER TABLE `inventory_voucher_lines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investors`
--
ALTER TABLE `investors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packets_issue_to_collectors`
--
ALTER TABLE `packets_issue_to_collectors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packets_issue_to_volunteers`
--
ALTER TABLE `packets_issue_to_volunteers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `priorities`
--
ALTER TABLE `priorities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rashan_packets`
--
ALTER TABLE `rashan_packets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `sample_packets`
--
ALTER TABLE `sample_packets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `volunteers`
--
ALTER TABLE `volunteers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `collectors`
--
ALTER TABLE `collectors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `collector_needs`
--
ALTER TABLE `collector_needs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `donation_packets`
--
ALTER TABLE `donation_packets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `expense_vouchers`
--
ALTER TABLE `expense_vouchers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inventories`
--
ALTER TABLE `inventories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `inventory_vouchers`
--
ALTER TABLE `inventory_vouchers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `inventory_voucher_lines`
--
ALTER TABLE `inventory_voucher_lines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `investors`
--
ALTER TABLE `investors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `packets_issue_to_collectors`
--
ALTER TABLE `packets_issue_to_collectors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `packets_issue_to_volunteers`
--
ALTER TABLE `packets_issue_to_volunteers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `priorities`
--
ALTER TABLE `priorities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `provinces`
--
ALTER TABLE `provinces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `rashan_packets`
--
ALTER TABLE `rashan_packets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sample_packets`
--
ALTER TABLE `sample_packets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `volunteers`
--
ALTER TABLE `volunteers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vouchers`
--
ALTER TABLE `vouchers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
