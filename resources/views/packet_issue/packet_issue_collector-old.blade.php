@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	     <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" id="app">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Packet Issue</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Packet Issue</li>
                        </ol>
                    </div>
                </div>
				<!-- .row -->
				
				<div class="row">
					<div class="col-sm-12">
						@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
					</div>
				</div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">

							<h4 class="card-title">Packet Issue To Collector</h4>
							<div class="row">
								<div class="col-sm-12 col-xs-12">
									<form action="{{ route('PostPacketsIssueCollector') }}" method="POST">
										@csrf()
										<div class="row">
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Province<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" name="province_id" style="padding: 0;" v-model="selected_province" required>
													<option>Select Province</option>
														<option v-for="province in Provinces" :key="province.id" :value="province.id" >@{{ province.name }}</option>
													</select>
												</div>
											</div>
											<div  class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">City<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" name="city_id" style="padding: 0;" v-model="selected_city" required>
													<option>Select City</option>
														<option v-for="city in ProvinceCities" :key="city.id" :value="city.id" >@{{ city.city_name }}</option>
													</select>
												</div>
											</div>
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Area<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" name="area_id" v-model="selected_area" style="padding: 0;">
													<option></option>
														<template v-for="Parea in CityAreas">
															<option :value="Parea.id" :key="Parea.id" disabled >@{{ Parea.name }}</option>
															<option v-for="area in Parea.inner_area" :value="area.id" :key="area.id" >@{{ area.name }}</option>
														</template>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div  class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Volunteer<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" name="volunteer_id" v-model="selected_volunteer" style="padding: 0;" required>
													<option>Select Volunteer</option>
														<option v-for="volunteer in AreaVolunteers" :key="volunteer.id" :value="volunteer.id" >@{{ volunteer.name }}</option>
													</select>
												</div>
											</div>
											<div  class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Collector<span class="text-danger">*</span></label>
												<div class="row">
													<v-select style="width: 100%" :options="AreaCollectors" label="name" code="id" @input="(val)=>{selected_collector_id = val? val.id : null}" required ></v-select>
													<input type="hidden" name="collector_id" v-model="selected_collector_id" required>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Rashan Packet<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" v-model="add_packet" style="padding: 0;">
													<option>Select Packet</option>
														<option v-for="(packet, k) in VolunteerPackets" :key="packet.id" :value="k" >@{{ packet.donation_packet.name }}</option>
													</select>
												</div>
												<a @click="selected_packets.push(VolunteerPackets[add_packet])" class="btn btn-info btn-block text-white"><i class="fa fa-plus"></i></a>
											</div>
										</div>
										<br>
										<br>
										<br>
										<div class="row">
											<div class="container">
												<table class="table table-bordered table-striped">
													<thead>
														<tr>
															<th>Name</th>
															<th>Qty</th>
															<th>Option</th>
														</tr>
													</thead>
													<tbody>
														<tr v-for="(packet, i) in selected_packets">
															<td>
																<input class="form-control" type="hidden" readonly v-model="packet.id" :name="'packets['+i+'][packets_issue_to_volunteer_id]'" required >
																<input class="form-control" type="hidden" readonly v-model="SelectedCollector.collector_need_id" :name="'packets['+i+'][collector_need_id]'" required >
																<input class="form-control" type="hidden" readonly v-model="SelectedCollector.collector_need_status" :name="'packets['+i+'][collector_need_status]'" required >
																@{{ packet.donation_packet.name }}
															</td>
															<td><input class="form-control" type="number" :max="packet.available" :name="'packets['+i+'][no_of_packets]'" required > </td>
															<td><a class="btn btn-sm" @click="selected_packets.splice(i,1)"><i class="fa fa-remove"></i></a></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div v-if="selected_packets.length" class="row">
											<div class="col-lg-6 m-t-20">
												<div class="input-group">
													<button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection

@section('script')
    <!-- use the latest vue-select release -->
    <script src="https://unpkg.com/vue-select@latest"></script>
    <link rel="stylesheet" href="https://unpkg.com/vue-select@latest/dist/vue-select.css">
<script>

   var App = new Vue({
	el: '#app',
	data: {
		selected_province: 0,
		selected_city: 0,
		selected_area: 0,
		selected_volunteer: 0,
		selected_packets: [],
		selected_collector_id: 0,
		add_packet: 0,
		
		Provinces: @json($Provinces, JSON_NUMERIC_CHECK),
		Cities: @json($Cities, JSON_NUMERIC_CHECK),
		Areas: @json($Areas, JSON_NUMERIC_CHECK),

		AreaVolunteers: [],
		AreaCollectors: [],
		AllPackets: [],

	},
	computed: {
		ProvinceCities: function(){
			return this.Cities.filter( (city)	=>	{ return city.province_id == this.selected_province });
		},
		CityAreas: function(){
			return this.Areas.filter( (area)	=>	{ return area.city_id == this.selected_city });
		},
		VolunteerPackets: function(){
			return this.AllPackets.filter( (packet)	=>	{ return packet.volunteer_id == this.selected_volunteer });
		},
		SelectedCollector: function(){
			return this.AreaCollectors.find((collector) => {return collector.id == this.selected_collector_id });
		}
	},
	watch: {
		selected_area: function(new_area){
			this.GetData(new_area);
		},
		selected_volunteer: function(newVal){
			this.selected_packets = [];
		}
	},
	methods:{
		GetData: function(area){
			$.get('{{ route('GetPacketsIssueCollectorData') }}', { area_id: this.selected_area})
			.done(function(res) {
				App.AreaVolunteers = res.Volunteers;
				App.AreaCollectors = res.Collectors;
				App.AllPackets = res.VolunteerPackets;
				console.log(res);
			});
		}
	},
	components: {
		'v-select': VueSelect.VueSelect,
	}
   });
   
</script>
@endsection
