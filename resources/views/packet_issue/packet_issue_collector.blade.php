@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	     <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" id="app">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Packet Issue</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Packet Issue</li>
                        </ol>
                    </div>
                </div>
				<!-- .row -->
				
				<div class="row">
					<div class="col-sm-12">
						@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
					</div>
				</div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">

							<h4 class="card-title">Packet Issue To Collector</h4>
							<div class="row">
								<div class="col-sm-12 col-xs-12">
									<form action="{{ route('PostPacketsIssueCollector') }}" method="POST">
										@csrf()
										<div class="row">
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Province<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" name="province_id" style="padding: 0;" v-model="selected_province" required>
													<option>Select Province</option>
														<option v-for="province in Provinces" :key="province.id" :value="province.id" >@{{ province.name }}</option>
													</select>
												</div>
											</div>
											<div  class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">City<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" name="city_id" style="padding: 0;" v-model="selected_city" required>
													<option>Select City</option>
														<option v-for="city in ProvinceCities" :key="city.id" :value="city.id" >@{{ city.city_name }}</option>
													</select>
												</div>
											</div>
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Area<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" name="area_id" v-model="selected_area" style="padding: 0;">
													<option></option>
														<template v-for="Parea in CityAreas">
															<option :value="Parea.id" :key="Parea.id" disabled >@{{ Parea.name }}</option>
															<option v-for="area in Parea.inner_area" :value="area.id" :key="area.id" >@{{ area.name }}</option>
														</template>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div  class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Volunteer<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" name="volunteer_id" v-model="selected_volunteer" style="padding: 0;" required>
														<option v-for="volunteer in AreaVolunteers" :value="volunteer.id" >@{{ volunteer.name }}</option>
													</select>
												</div>
											</div>
											<div  class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Collector<span class="text-danger">*</span></label>
												<div class="input-group">
													<select id="select2" class="form-control" required>
														<option></option>
														<option v-for="collector in AreaCollectors" :value="collector.id">@{{ collector.name +" | "+ collector.cnic }}</option>
													</select>
													<template v-if="SelectedCollector">
														<input type="hidden" name="collector_id" v-model="SelectedCollector.id" required>
														<input type="hidden" name="collector_need_id" v-model="SelectedCollector.collector_need_id" required>
													</template>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Rashan Packet<span class="text-danger">*</span></label>
												<div class="input-group">
													<select class="form-control" name="packets_issue_to_volunteer_id" v-model="selected_packet" style="padding: 0;">
															<option v-for="(packet, k) in VolunteerPackets" :value="packet.id" >@{{ packet.donation_packet.name }}</option>
													</select>
												</div>
											</div>
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">No Of Packets<span class="text-danger">*</span> <span class="text-info">( @{{ SelectedPacketLimit - no_of_packets }} )</span> </label>
												<div class="input-group">
													<input type="number" v-model="no_of_packets" class="form-control" name="no_of_packets" min="1" :max="SelectedPacketLimit" required>
												</div>
											</div>
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Next Collection In<span class="text-danger">*</span> </label>
												<div class="input-group">
													<select name="next_collection_in" class="form-control">
														<option value="1">1 WEEK</option>
														<option value="2">2 WEEK</option>
														<option value="3">3 WEEK</option>
														<option value="4">4 WEEK</option>
														<option value="completed">Completed</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6 m-t-20">
												<div class="input-group">
													<button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection

@section('script')
<script>

   var App = new Vue({
	el: '#app',
	data: {
		selected_province: 0,
		selected_city: 0,
		selected_area: 0,
		selected_volunteer: 0,
/* 		selected_collector_id: 0, */
		SelectedCollector: {},
		selected_packet: 0,
		no_of_packets: 0,
		
		Provinces: @json($Provinces, JSON_NUMERIC_CHECK),
		Cities: @json($Cities, JSON_NUMERIC_CHECK),
		Areas: @json($Areas, JSON_NUMERIC_CHECK),

		AreaVolunteers: [],
		AreaCollectors: [],
		AllPackets: [],

	},
	computed: {
		ProvinceCities: function(){
			return this.Cities.filter( (city)	=>	{ return city.province_id == this.selected_province });
		},
		CityAreas: function(){
			return this.Areas.filter( (area)	=>	{ return area.city_id == this.selected_city });
		},
		VolunteerPackets: function(){
			return this.AllPackets.filter( (packet)	=>	{ return packet.volunteer_id == this.selected_volunteer });
		},
/* 		SelectedCollector: function(){
			return this.AreaCollectors.find((collector) => {return collector.id == this.selected_collector_id });
		} */
		SelectedPacketLimit: function(){
			if(this.VolunteerPackets.length && this.selected_packet){
				return this.VolunteerPackets.find((packet) => { return packet.id == this.selected_packet }).available;
			}
			return 0;
		}
	},
	watch: {
		selected_area: function(new_area){
			this.GetData(new_area);
		},
	},
	methods:{
		GetData: function(area){
			if ($('#select2').data('select2')) {
				$('#select2').select2('destroy');
			}
			$.get('{{ route('GetPacketsIssueCollectorData') }}', { area_id: this.selected_area})
			.done(function(res) {
				App.AreaVolunteers = res.Volunteers;
				App.AreaCollectors = res.Collectors;
				App.AllPackets = res.VolunteerPackets;
				$("#select2").select2().on("select2:select", function (e) { App.setSelectedCollector(e.target.value); });
			});
		},
		setSelectedCollector: function(id){
			this.SelectedCollector = this.AreaCollectors.find((collector) => { return collector.id == id });
		},
	},
	components: {

	}
   });
   
</script>
@endsection
