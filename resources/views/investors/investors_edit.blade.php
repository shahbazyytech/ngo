@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<div class="row page-titles">
			<div class="col-md-5 col-8 align-self-center">
				<h3 class="text-themecolor m-b-0 m-t-0">Collector</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">Collector</li>
				</ol>
			</div>
		</div>
		<!-- .row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="card card-body">
					<h4 class="card-title">Investors</h4>
					<div class="row">
						@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
                        @endif

						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
						<div class="col-sm-12 col-xs-12">
							<form method="post" action="{{ route('PostEditInvestors', $Investor->id) }}">
								@csrf()
								<div class="row">
									<div class="col-lg-6">
										<label class="control-label m-t-20" for="example-input1-group2">Name<span class="text-danger">*</span></label>
										<div class="input-group">
											<input type="text" class="form-control" name="name" value="{{ $Investor->name }}" required>
										</div>
									</div>
									<div class="col-lg-6">
										<label class="control-label m-t-20" for="example-input1-group2">CNIC<span class="text-danger">*</span></label>
										<div class="input-group">
											<input type="number" class="form-control" name="cnic" data-mask="9999999999999" required value="{{ $Investor->cnic }}">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-4">
										<label class="control-label m-t-20" for="example-input1-group2">Contact Number<span class="text-danger">*</span></label>
										<div class="input-group">
											<input type="number" class="form-control" name="contact_no" data-mask="9999999999" value="{{ $Investor->contact_no }}" required>
										</div>
									</div>
									<div class="col-lg-4">
										<label class="control-label m-t-20" for="example-input1-group2">Amount<span class="text-danger">*</span></label>
										<div class="input-group">
											<input type="number" class="form-control" name="amount" data-mask="9999999999" value="{{ $Investor->amount }}" required>
										</div>
									</div>
									<div class="col-lg-4">
										<label class="control-label m-t-20" for="example-input1-group2">Payment mode <span class="text-danger">*</span></label>
										<div class="input-group">
											<select class="form-control" name="payment_mod" required>
												<option value="">---</option>
												<option value="cash" {{($Investor->payment_mod == 'cash')? "selected": ""}}>Cash</option>
												<option value="bank" {{($Investor->payment_mod == 'bank')? "selected": ""}}>Bank</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-3">
										<label class="control-label m-t-20"for="example-input1-group2">Future Contact</label>
										<div class="switch"  style="margin-top:20px" >
											<label>NO
												<input type="checkbox" value="1" name="future_contact" {{($Investor->future_contact == 1)? "checked": ""}} ><span class="lever"></span>YES</label>
										</div>
									</div>
									<div class="col-lg-8">
										<label class="control-label m-t-20" for="example-input1-group2">Address<span class="text-danger">*</span></label>
										<div class="input-group">
											<textarea class="form-control" rows="5" name="address" required> {{ $Investor->address }} </textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6 m-t-20">
										<div class="input-group">
											<button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
		<!-- ============================================================== -->
		<!-- End Page Content -->
		<!-- ============================================================== -->

	</div>


	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection
