@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<div class="row page-titles">
			<div class="col-md-5 col-8 align-self-center">
				<h3 class="text-themecolor m-b-0 m-t-0">List Inventory Vouchers</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">List Inventory Vouchers</li>
				</ol>
			</div>
			
		</div>
		<!-- ============================================================== -->
		<!-- End Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
                        @endif

						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
						<h4 class="card-title">List Vouchers</h4>
						<div class="table-responsive m-t-40">
							<table id="myTable" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Id</th>
										<th>Voucher No</th>
										<th>Type</th>
										<th>Cost</th>
										<th>Description</th>
										<th>Created At</th>
										<th class="text-nowrap">Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($Vouchers as $Voucher)
									<tr>
										<td>{{ $Voucher->id }}</td>
										<td>{{ $Voucher->voucher_no }}</td>
										<td class="text-capitalize">{{ $Voucher->type }}</td>
										<td>{{ $Voucher->total_cost }}</td>
										<td>{{ $Voucher->description }}</td>
										<td>{{ $Voucher->created_at }}</td>
										<td class="text-nowrap">
										<a href="{{ route('GetEditInventoryVoucher', $Voucher->id) }}" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
	</div>

	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection
@section('script')
<script>
	$("#myTable").dataTable();
</script>
@endsection