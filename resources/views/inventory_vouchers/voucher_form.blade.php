@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	     <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" id="app">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Inventory Voucher</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Inventory Voucher</li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
							@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
                        @endif

						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
                            <h4 class="card-title">Inventory Voucher</h4>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form action="{{ route('CreateInventoryVoucher') }}" method="POST">
									    @csrf()
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Voucher No<span class="text-danger">*</span></label>
                                                <div class="input-group">
												<input type="text" class="form-control" name="voucher_no" placeholder="Voucher No" value="{{ date('Y').'-'.time() }}" required>
                                                </div>
                                            </div>
                                            <div v-show="false" class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Type<span class="text-danger">*</span></label>
                                                <div class="input-group">
													<select class="form-control" v-model="type" name="type" required>
														<option value="receive">Purchase</option>
														<option value="donate">Issue</option>
													</select>
                                                </div>
                                            </div>
										</div>
										<div class="row">
											<div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Select Inventory<span class="text-danger"> * </span></label>
                                                <div class="input-group">
													<select id="select2" class="form-control" v-model="selected_item">
														<option></option>
													<option v-for="Item in Items" :value="Item.id">@{{ Item.name+' | '+Item.qty+' '+Item.unit }}</option>
													</select>
													<button type="button" :disabled="selected_item == null" class="btn btn-info waves-effect text-white" @click="addSelectedItems" ><i class="fa fa-plus"></i></button>
                                                </div>
											</div>
                                            <div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Description</label>
                                                <div class="input-group">
                                                    <textarea class="form-control" rows="5" name="description" placeholder="Description"></textarea>
                                                </div>
											</div>
										</div>
										<br>
										<br>
										<br>
										<div v-if="type" class="row">
											<div class="container">
												<table class="table table-bordered table-striped">
													<thead>
														<tr>
															<th>Name</th>
															<th>Qty</th>
															<th v-show="type=='receive'">Unit Cost</th>
															<th v-show="type=='receive'">Total Cost</th>
															<th class="text-capitalize">After @{{ type }} Qty</th>
															<th>Option</th>
														</tr>
													</thead>
													<tbody>
														<tr v-for="(item, i) in SelectedItems">
															<td><input class="form-control" type="hidden" v-model="item.id" :name="'items['+i+'][item_id]'" required > @{{ item.name }} </td>
															<td>
																<input v-if="type == 'receive'" class="form-control" v-model.number="item.input_qty" type="number" min="1" :name="'items['+i+'][qty]'" required >
																<input v-else class="form-control" type="number" v-model.number="item.input_qty" min="1" :max="item.qty" :name="'items['+i+'][qty]'" required >
															</td>
															<td v-show="type=='receive'">
																<input class="form-control" v-model.number="item.unit_cost" type="number" :name="'items['+i+'][unit_cost]'" :required="type=='receive'" >
															</td>
															<td v-show="type=='receive'">
																@{{ (item.unit_cost * item.input_qty) }} Rs
															</td>
															<td>
																<input class="form-control" :value="calc(item.qty + operators[type] + item.input_qty)" type="hidden" :name="'items['+i+'][updated_qty]'" >
																@{{ calc(item.qty + operators[type] + item.input_qty) }} @{{ item.unit }}
															</td>
															<td><a class="btn btn-sm" @click="SelectedItems.splice(i,1)"><i class="fa fa-remove"></i></a></td>
														</tr>
														<tr v-show="type=='receive'">
															<td class="text-right" colspan="2">
																Total
															</td>
															<td>
																<input type="hidden" name="total_cost" :value="total_cost">
																@{{ total_cost }} Rs
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>

                                        <div class="row">
                                            <div class="col-lg-6 m-t-20">
                                                <div class="input-group">
                                                    <button :disabled="SelectedItems.length == 0" type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                                </div>
                                            </div>
										</div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection

@section('script')
<script>

   var App = new Vue({
	el: '#app',
	data: {
		type: 'receive',
		selected_item: null,
		SelectedItems: [],
		operators: {receive: '+', donate: '-'},
		Items: @json($Items, JSON_NUMERIC_CHECK),
	},
	mounted: function(){
		$("#select2").select2().on("select2:select", (e) => this.setSelectedItem(e.target.value) );
	},
	computed: {
		total_cost: function(){
			return this.SelectedItems.reduce((t, i) => { return t+(i.unit_cost * i.input_qty) }, 0);
		}
	},
	watch: {
		type: function(newVal){
			this.SelectedItems = [];
		}
	},
	methods: {
		setSelectedItem: function(id){
			this.selected_item = this.Items.find((item) => { return item.id == id });
		},
		addSelectedItems: function(){
			let id = this.SelectedItems.find((item) =>  item.id == this.selected_item.id);
			if(id == null){
				this.SelectedItems.push({ ...this.selected_item, input_qty: 0, unit_cost: 0 });
			} else {
				alert('Item already added in list!');	
			}
			this.selected_item = null
		},
		calc: function(value){
			return  eval(value);
		},
		updatedInventoryRate: function({rate, qty, unit_cost, input_qty}){
			let itemTotAmount = (qty*rate);
			let newTotAmount = (input_qty*unit_cost);
			let TotalAmount = (itemTotAmount+newTotAmount);
			let TotalQty= (qty+input_qty);
			return (TotalAmount/TotalQty).toFixed(3);
		}

	}
   });
   
</script>
@endsection