@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid" id="app">
		<!-- ============================================================== -->
		<!-- Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<div class="row page-titles">
			<div class="col-md-5 col-8 align-self-center">
				<h3 class="text-themecolor m-b-0 m-t-0">Inventory Item</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">Inventory Item</li>
				</ol>
			</div>
		</div>
		<!-- .row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="card card-body">
				<h4 class="card-title">Items {{ $Item->name }}</h4>
					<div class="row">
						@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
                        @endif

						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
						<div class="col-sm-12 col-xs-12">

							<form method="post" action="{{ route('PostEditItem', $Item->id) }}">
								@csrf()
								<div class="row">
									<div class="col-lg-4">
										<label class="control-label m-t-20" for="example-input1-group2">Name<span class="text-danger">*</span></label>
										<div class="input-group">
											<input type="text" class="form-control" name="name" placeholder="Enter Name" value="{{ $Item->name }}" required>
										</div>
									</div>
									<div class="col-lg-4 d-none">
										<label class="control-label m-t-20" for="example-input1-group2">Qty<span class="text-danger">*</span></label>
										<div class="input-group">
											<input type="number" class="form-control" name="qty" placeholder="Qty" value="{{$Item->qty}}" >
										</div>
									</div>
									<div class="col-lg-4">
										<label class="control-label m-t-20" for="example-input1-group2">Unit<span class="text-danger">*</span></label>
										<div class="input-group">
										<input type="text" class="form-control" name="unit" placeholder="KG,Pices,etc..." value="{{ $Item->unit }}" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6 d-none">
										<label class="control-label m-t-20" for="example-input1-group2">Rate <span class="text-danger">*</span></label>
										<input type="number" step="0.001" class="form-control" value="{{ $Item->rate }}" name="rate" required>
									</div>
									<div class="col-lg-6">
										<label class="control-label m-t-20" for="example-input1-group2">Remarks</label>
										<div class="input-group">
										<textarea class="form-control" rows="5" name="remarks" placeholder="Remarks">{{ $Item->remarks }}</textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6 m-t-20">
										<div class="input-group">
											<button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
		<!-- ============================================================== -->
		<!-- End Page Content -->
		<!-- ============================================================== -->

	</div>


	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection
@section('script')
<script>

   var App = new Vue({
	el: '#app',
	data: {

	},
	computed: {

	}
   });
   
</script>
@endsection
