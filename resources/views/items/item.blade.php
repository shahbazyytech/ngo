@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<div class="row page-titles">
			<div class="col-md-5 col-8 align-self-center">
				<h3 class="text-themecolor m-b-0 m-t-0">List Inventory Items</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">List Inventory Items</li>
				</ol>
			</div>
			
		</div>
		<!-- ============================================================== -->
		<!-- End Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
                        @endif

						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
						<h4 class="card-title">List Inventory</h4>
						<div class="table-responsive m-t-40">
							<table id="myTable" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Name</th>
										<th>Qty</th>

										<th>Remarks</th>
										<th>Created At</th>
										<th class="text-nowrap">Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($Items as $Item)
									<tr>
										<td>{{ $Item->name }}</td>
										<td>{{ $Item->qty.' '.$Item->unit }}</td>

										<td>{{ $Item->remarks }}</td>
										<td>{{ $Item->created_at }}</td>
										<td class="text-nowrap">
										<a href="{{ route('GetEditItem', $Item->id) }}" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
	</div>

	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection
@section('script')
<script>
	$("#myTable").dataTable({
/* 
		dom: 'Bfrtip',
		buttons: [
			'print'
		],

		processing: true,
		serverSide: true,
		lengthChange: true,
		option: ['pageLength'],
		pageLength: 10,
		ajax: '{!! route('Items') !!}',
//				lengthMenu: [ 10, 20 ],
		columns: [
			{data: 'id', name: 'id'},
			{data: 'name', name: 'name'},
			{data: 'qty', name: 'qty'},
			{data: 'remarks', name: 'remarks'},
			{data: 'created_at', name: 'created_at'},
			{data: 'actions', name: 'actions', searchable: false, orderable: false},
		]
 */
	});
</script>
@endsection