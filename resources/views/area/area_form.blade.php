@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	     <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" id="app">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Area</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Area</li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
							@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
                            <h4 class="card-title">Area</h4>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form action="{{ route('CreateArea') }}" method="POST">
									    @csrf()
                                        <div class="row">
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Province<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" name="province_id" style="padding: 0;" v-model="selected_province" required>
													<option>Select Province</option>
														<option v-for="province in Provinces" :key="province.id" :value="province.id" >@{{ province.name }}</option>
													</select>
												</div>
											</div>
											<div  class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">City<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" name="city_id" style="padding: 0;" v-model="selected_city" required>
													<option>Select City</option>
														<option v-for="city in ProvinceCities" :key="city.id" :value="city.id" >@{{ city.city_name }}</option>
													</select>
												</div>
											</div>
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Area <small><span class="text-info"> (if-parent)</span></small></label>
												<div class="input-group">
												<select class="form-control" name="parent_id" style="padding: 0;">
													<option></option>
														<option v-for="area in CityAreas" :key="area.id" :value="area.id" >@{{ area.name }}</option>
													</select>
												</div>
											</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="control-label m-t-20" for="example-input1-group2">Name<span class="text-danger">*</span></label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="name" placeholder="Enter Name" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="control-label m-t-20" for="example-input1-group2">Map latitudes<span class="text-danger">*</span></label>
                                                <div class="input-group">
                                                    <input type="number" class="form-control" step="0.01" name="map_lat" placeholder="Enter value in decimal" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="control-label m-t-20" for="example-input1-group2">Map Longitude<span class="text-danger">*</span></label>
                                                <div class="input-group">
                                                    <input type="number" class="form-control" step="0.01" name="map_lng" placeholder="Enter value in decimal" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 m-t-20">
                                                <div class="input-group">
                                                    <button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection

@section('script')
<script>

   var App = new Vue({
	el: '#app',
	data: {
		selected_province: 0,
		selected_city: 0,
		Provinces: @json($Provinces, JSON_NUMERIC_CHECK),
		Cities: @json($Cities, JSON_NUMERIC_CHECK),
		Areas: @json($Areas, JSON_NUMERIC_CHECK)
	},
	computed: {
		ProvinceCities: function(){
			return this.Cities.filter( (city)	=>	{ return city.province_id == this.selected_province });
		},
		CityAreas: function(){
			return this.Areas.filter( (area)	=>	{ return area.city_id == this.selected_city });
		},
	}
   });
   
</script>
@endsection
