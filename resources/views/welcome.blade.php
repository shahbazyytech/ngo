@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid" id="app">
		<!-- ============================================================== -->
		<!-- Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<div class="row page-titles">
			<div class="col-md-5 col-8 align-self-center">
				<h3 class="text-themecolor">Dashboard</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">Dashboard</li>
				</ol>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<!-- Row -->

		<div class="row">

			<div class="col-md-6 col-lg-3">
				<div class="card card-body">
					<!-- Row -->
					<div class="d-flex flex-row">
						<!-- Column -->
						<div class="round round-lg text-white d-inline-block text-center rounded-circle bg-danger">
							<i class="fa fa-users"></i>
						</div>
						<div class="ml-2 align-self-center">
							<h2 class="font-weight-light mb-0">{{ $ActiveNeedCount }} | {{ $CollectorCount }}</h2>
							<h6 class="text-muted">Needy Persons</h6>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-lg-3">
				<div class="card card-body">
					<!-- Row -->
					<div class="d-flex flex-row">
						<!-- Column -->
						<div class="round round-lg text-white d-inline-block text-center rounded-circle bg-danger">
							<i class="fa fa-user"></i>
						</div>
						<div class="ml-2 align-self-center">
							<h2 class="font-weight-light mb-0">{{ $VolunteersCount }}</h2>
							<h6 class="text-muted">Volunteers</h6>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-lg-3">
				<div class="card card-body">
					<!-- Row -->
					<div class="d-flex flex-row">
						<div class="round round-lg text-white d-inline-block text-center rounded-circle bg-danger">
							<i class="ti-money"></i>
						</div>
						<!-- Column -->
						<div class="ml-2 align-self-center">
							<h2 class="font-weight-light mb-0">{{ $SumDonationReceive }} Rs.</h2>
							<h6 class="text-muted">Donations</h6>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-lg-3">
				<div class="card card-body">
					<!-- Row -->
					<div class="d-flex flex-row">
						<div class="round round-lg text-white d-inline-block text-center rounded-circle bg-danger">
							<i class="ti-money"></i>
						</div>
						<!-- Column -->
						<div class="ml-2 align-self-center">
							<h2 class="font-weight-light mb-0">{{ (int) $SumExpenses }} Rs.</h2>
							<h6 class="text-muted">Expenses</h6>
						</div>
					</div>
				</div>
			</div>


			<div class="col-md-6 col-lg-3">
				<div class="card card-body">
					<!-- Row -->
					<div class="d-flex flex-row">
						<div class="round round-lg text-white d-inline-block text-center rounded-circle bg-danger">
							<i class="ti-money"></i>
						</div>
						<!-- Column -->
						<div class="ml-2 align-self-center">
							<h2 class="font-weight-light mb-0">{{ (int) $SumInventoryReceived }} Rs.</h2>
							<h6 class="text-muted">Inventory Received</h6>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-lg-3">
				<div class="card card-body">
					<!-- Row -->
					<div class="d-flex flex-row">
						<div class="round round-lg text-white d-inline-block text-center rounded-circle bg-danger">
							<i class="fa fa-users"></i>
						</div>
						<!-- Column -->
						<div class="ml-2 align-self-center">
							<h2 class="font-weight-light mb-0">{{ $DonorsCount }}</h2>
							<h6 class="text-muted">Donors</h6>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-12">
								<div class="d-flex flex-wrap">
									<div>
										<h3 class="card-title">Monthly Donation Received</h3>
									</div>
									<div class="ml-auto">
										<ul class="list-inline">
											<li class="list-inline-item px-2">

											</li>
											<li class="list-inline-item px-2">
												<h6 class="text-info"><i class="fa fa-circle font-10 mr-2"></i>Received</h6>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-12">
								<div class="amp-pxl" id="amp-pxl" style="height: 360px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-12">
								<div class="d-flex flex-wrap">
									<div>
										<h3 class="card-title">Monthly Expenses</h3>
									</div>
									<div class="ml-auto">
										<ul class="list-inline">
											<li class="list-inline-item px-2">

											</li>
											<li class="list-inline-item px-2">
												<h6 class="text-info"><i class="fa fa-circle font-10 mr-2"></i>Expenses</h6>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-12">
								<div id="monthly_expense" style="height: 360px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-12">
								<div class="d-flex flex-wrap">
									<div>
										<h3 class="card-title">Monthly Inventory Purchase</h3>
									</div>
									<div class="ml-auto">
										<ul class="list-inline">
											<li class="list-inline-item px-2">

											</li>
											<li class="list-inline-item px-2">
												<h6 class="text-info"><i class="fa fa-circle font-10 mr-2"></i>Expenses</h6>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-12">
								<div id="monthly_inventory_purchase" style="height: 360px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-8">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-12">
								<div class="d-flex flex-wrap">
									<div>
										<h3 class="card-title">Distributions of ({{ now()->subMonth(1)->format('F-Y') }} - {{ now()->format('F-Y') }})</h3>
									</div>
									<div class="ml-auto">
										<ul class="list-inline">
{{-- 											<li class="list-inline-item px-2">
												<h6 class="text-success"><i class="fa fa-circle font-10 mr-2 "></i>No of Ration Packets</h6>
											</li> --}}
											<li class="list-inline-item px-2">
												<h6 class="text-info"><i class="fa fa-circle font-10 mr-2"></i>No of donation</h6>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-12">
								<div id="week-distribution" class="amp-pxl" style="height: 360px;"></div>
							</div>						
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-4">
				<div class="card">
					<div class="card-body">
						<h3 class="card-title">Donations </h3>

						<div>
							<canvas id="chart3" height="150"></canvas>
						</div>

					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-4">
				<div class="card">
					<div class="card-body">
						<h3 class="card-title">City Wise Beneficiaries</h3>

						<div>
							<canvas id="city_wise_beneficiaries" height="150"></canvas>
						</div>

					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="card">
					<div class="card-body">
						<h3 class="card-title">Priority Wise Beneficiaries</h3>

						<div>
							<canvas id="priority_wise_beneficiaries" height="150"></canvas>
						</div>

					</div>
				</div>
			</div>
		</div>

		<!-- Row -->
		
		
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection
@section('script')

<script>
	var chart = [];
	var App = new Vue({
		el: '#app',
		data: {
			MonthlyDonation: @json($MonthlyDonation, JSON_NUMERIC_CHECK),
			MonthlyExpense: @json($MonthlyExpense, JSON_NUMERIC_CHECK),
			InventoryPurchase: @json($InventoryPurchase, JSON_NUMERIC_CHECK),
			WeeksOfDonation: @json($WeeksOfDonation, JSON_NUMERIC_CHECK),
			SumByType: @json($SumByType, JSON_NUMERIC_CHECK),
			CollectorCountCityWise: @json($CollectorCountCityWise, JSON_NUMERIC_CHECK),
			CollectorCountPriorityWise: @json($CollectorCountPriorityWise, JSON_NUMERIC_CHECK)
//			CityWisePieChartData: null,
		},
		computed: {
			maxMonthlyDonation: function(){
				return 	_.max(this.MonthlyDonation);
			},
			maxMonthlyExpense: function(){
				return 	_.max(this.MonthlyExpense);
			},
			maxMonthlyInventoryPurchase: function(){
				return 	_.max(this.InventoryPurchase);
			},
			getCounts: function(){
				return _.pluck(this.WeeksOfDonation, 'no_count');
			},
			getCountsWeeks: function(){
				return _.pluck(this.WeeksOfDonation, 'week');
			},
			getNoOfPacketsWeeks: function(){
				return _.pluck(this.WeeksOfDonation, 'no_of_packets');
			},
			maxgetCountsWeeks: function(){
				return _.max(_.pluck(this.WeeksOfDonation, 'no_of_packets'));
			},
			getSumByType: function(){
				return this.SumByType.map(function(row){
					return {
						value: row.amount,
						color: (row.zakat)? '#009efb' : '#7460ee',
						highlight: (row.zakat)? '#009efb' : '#7460ee',
						label: (row.zakat)? 'Zakat' : 'others',
					}
				})
			},
			CityWisePieChartData: function(){
				return  this.CollectorCountCityWise.map((row) => {
					var randomColor = '#'+Math.floor(Math.random()*16777215).toString(16);
					return {
						value: row.no_count,
						color: randomColor,
						highlight: randomColor,
						label: row.city_name,
					}
				});
			},
			PriorityWisePieChartData: function(){
				return  this.CollectorCountPriorityWise.map((row) => {
					var randomColor = '#'+Math.floor(Math.random()*16777215).toString(16);
					return {
						value: row.no_count,
						color: randomColor,
						highlight: randomColor,
						label: row.title,
					}
				});
			}
		},
		mounted() {
			new Chart(document.getElementById("city_wise_beneficiaries").getContext("2d")).Pie(this.CityWisePieChartData,{
				segmentShowStroke : true,
				segmentStrokeColor : "#fff",
				segmentStrokeWidth : 0,
				animationSteps : 100,
				tooltipCornerRadius: 0,
				animationEasing : "easeOutBounce",
				animateRotate : true,
				animateScale : false,
				legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
				responsive: true
			});

			new Chart(document.getElementById("priority_wise_beneficiaries").getContext("2d")).Pie(this.PriorityWisePieChartData,{
				segmentShowStroke : true,
				segmentStrokeColor : "#fff",
				segmentStrokeWidth : 0,
				animationSteps : 100,
				tooltipCornerRadius: 0,
				animationEasing : "easeOutBounce",
				animateRotate : true,
				animateScale : false,
				legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
				responsive: true
			});

		},
		methods: {

		},
	})

	$(function(){

		chart[1] = new Chartist.Bar('.amp-pxl', {
			labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        	series: [
				[
					{{$MonthlyDonation['Jan']}},
					{{$MonthlyDonation['Feb']}},
					{{$MonthlyDonation['Mar']}},
					{{$MonthlyDonation['Apr']}},
					{{$MonthlyDonation['May']}},
					{{$MonthlyDonation['Jun']}},
					{{$MonthlyDonation['Jul']}},
					{{$MonthlyDonation['Aug']}},
					{{$MonthlyDonation['Sep']}},
					{{$MonthlyDonation['Oct']}},
					{{$MonthlyDonation['Nov']}},
					{{$MonthlyDonation['Dec']}},
				]
        	]
		}, {
          axisX: {
            // On the x-axis start means top and end means bottom
            position: 'end',
            showGrid: false
          },
          axisY: {
            // On the y-axis start means left and end means right
            position: 'start'
          },
        high: App.maxMonthlyDonation+500,
        low: '0',
        plugins: [
            Chartist.plugins.tooltip()
        ]
		});

		chart[2] = new Chartist.Bar('#monthly_expense', {
			labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
			series: [
				[
					{{$MonthlyExpense['Jan']}},
					{{$MonthlyExpense['Feb']}},
					{{$MonthlyExpense['Mar']}},
					{{$MonthlyExpense['Apr']}},
					{{$MonthlyExpense['May']}},
					{{$MonthlyExpense['Jun']}},
					{{$MonthlyExpense['Jul']}},
					{{$MonthlyExpense['Aug']}},
					{{$MonthlyExpense['Sep']}},
					{{$MonthlyExpense['Oct']}},
					{{$MonthlyExpense['Nov']}},
					{{$MonthlyExpense['Dec']}},
				]
			]
		}, {
			axisX: {
			// On the x-axis start means top and end means bottom
			position: 'end',
			showGrid: false
			},
			axisY: {
			// On the y-axis start means left and end means right
			position: 'start'
			},
		high: App.maxMonthlyExpense+500,
		low: '0',
		plugins: [
			Chartist.plugins.tooltip()
		]
		});

		chart[2] = new Chartist.Bar('#monthly_inventory_purchase', {
			labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
			series: [
				[
					{{$InventoryPurchase['Jan']}},
					{{$InventoryPurchase['Feb']}},
					{{$InventoryPurchase['Mar']}},
					{{$InventoryPurchase['Apr']}},
					{{$InventoryPurchase['May']}},
					{{$InventoryPurchase['Jun']}},
					{{$InventoryPurchase['Jul']}},
					{{$InventoryPurchase['Aug']}},
					{{$InventoryPurchase['Sep']}},
					{{$InventoryPurchase['Oct']}},
					{{$InventoryPurchase['Nov']}},
					{{$InventoryPurchase['Dec']}},
				]
			]
		}, {
			axisX: {
			// On the x-axis start means top and end means bottom
			position: 'end',
			showGrid: false
			},
			axisY: {
			// On the y-axis start means left and end means right
			position: 'start'
			},
		high: App.maxMonthlyInventoryPurchase+500,
		low: '0',
		plugins: [
			Chartist.plugins.tooltip()
		]
		});
		

		chart[0] = new Chartist.Bar('#week-distribution', {
			labels: App.getCountsWeeks,
			series: [
				App.getCounts,
//				App.getNoOfPacketsWeeks
			]
			}, {
			axisX: {
				// On the x-axis start means top and end means bottom
				position: 'end',
				showGrid: false
			},
			axisY: {
				// On the y-axis start means left and end means right
				position: 'start'
			},
			high: App.maxgetCountsWeeks+1,
			low: '0',
			plugins: [
				Chartist.plugins.tooltip()
			]
		});
		for (var i = 0; i < chart.length; i++) {
			chart[i].on('draw', function(data) {
				if (data.type === 'line' || data.type === 'area') {
					data.element.animate({
						d: {
							begin: 500 * data.index,
							dur: 500,
							from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
							to: data.path.clone().stringify(),
							easing: Chartist.Svg.Easing.easeInOutElastic
						}
					});
				}
				if (data.type === 'bar') {
					data.element.animate({
						y2: {
							dur: 500,
							from: data.y1,
							to: data.y2,
							easing: Chartist.Svg.Easing.easeInOutElastic
						},
						opacity: {
							dur: 500,
							from: 0,
							to: 1,
							easing: Chartist.Svg.Easing.easeInOutElastic
						}
					});
				}
			});

		}

		var ctx3 = document.getElementById("chart3").getContext("2d");
		
		var myPieChart = new Chart(ctx3).Pie(App.getSumByType,{
			segmentShowStroke : true,
			segmentStrokeColor : "#fff",
			segmentStrokeWidth : 0,
			animationSteps : 100,
			tooltipCornerRadius: 0,
			animationEasing : "easeOutBounce",
			animateRotate : true,
			animateScale : false,
			legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
			responsive: true
		});

	});

</script>

@endsection

