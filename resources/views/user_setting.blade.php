@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<div class="row page-titles">
			<div class="col-md-5 col-8 align-self-center">
				<h3 class="text-themecolor">Dashboard</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">Dashboard</li>
				</ol>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<!-- Row -->

		@if($errors->any())
			<div class="row">
				<div class="col-12">
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>
		@endif

		@if(session('success'))
		<div class="row">
			<div class="col-12">
			<div class="alert alert-success">
				<p>{{ session('success') }}</p>
			</div>
			</div>
		</div>
		@endif

		<div class="row">
			<div class="col-sm-12">

				<div class="card card-body">
					<h4 class="card-title">Profile</h4>

					<form action="{{ route('PostUpdateInfo') }}" method="POST">
						@csrf()

						<div class="row">
							<div class="col-lg-4">
								<label class="control-label m-t-20" for="example-input1-group2">Name<span class="text-danger">*</span></label>
								<div class="input-group">
								<input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" placeholder="Enter Name" required>
								</div>
							</div>
							<div class="col-lg-4">
								<label class="control-label m-t-20" for="example-input1-group2">Email<span class="text-danger">*</span></label>
								<div class="input-group">
								<input type="text" class="form-control" name="email" value="{{ Auth::user()->email }}" placeholder="Enter Email" required>
								</div>
							</div>
							<div class="col-lg-4">
								<label class="control-label m-t-20" for="example-input1-group2">Current Password<span class="text-danger">*</span></label>
								<div class="input-group">
									<input type="password" class="form-control" name="password" placeholder="Enter Password" required>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-6 m-t-20">
								<div class="input-group">
									<button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Update</button>
								</div>
							</div>
						</div>

					</form>	
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-sm-12">

				<div class="card card-body">
					<h4 class="card-title">Profile</h4>

					<form action="{{ route('PostUpdatePwd') }}" method="POST">
						@csrf()

						<div class="row">
							<div class="col-lg-4">
								<label class="control-label m-t-20" for="example-input1-group2">Current Password<span class="text-danger">*</span></label>
								<div class="input-group">
								<input type="password" class="form-control" name="current_password" required>
								</div>
							</div>
							<div class="col-lg-4">
								<label class="control-label m-t-20" for="example-input1-group2">New Password<span class="text-danger">*</span></label>
								<div class="input-group">
								<input type="password" class="form-control" name="new_password" required>
								</div>
							</div>
							<div class="col-lg-4">
								<label class="control-label m-t-20" for="example-input1-group2">Confirm Password<span class="text-danger">*</span></label>
								<div class="input-group">
									<input type="password" class="form-control" name="new_password_confirmation" required>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-6 m-t-20">
								<div class="input-group">
									<button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Update</button>
								</div>
							</div>
						</div>

					</form>	
				</div>
			</div>
		</div>


		
		
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection
