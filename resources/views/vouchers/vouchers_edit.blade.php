@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<div class="row page-titles">
			<div class="col-md-5 col-8 align-self-center">
				<h3 class="text-themecolor m-b-0 m-t-0">Vouchers</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">Vouchers</li>
				</ol>
			</div>
		</div>
		<!-- .row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="card card-body">
					<h4 class="card-title">Vouchers</h4>
					<div class="row">
						@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
                        @endif

						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
						<div class="col-sm-12 col-xs-12">
							<form method="post" action="{{ route('PostEditVouchers', $Vouchers->id) }}">
								@csrf()
								<div class="row">
                                            <div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Name<span class="text-danger">*</span></label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="name" value="{{ $Vouchers->name }}" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Amount<span class="text-danger">*</span></label>
                                                <div class="input-group">
                                                    <input type="number" class="form-control" name="amount" value="{{ $Vouchers->amount }}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Date<span class="text-danger">*</span></label>
                                                <div class="input-group">
                                                    <input type="date" class="form-control" name="date" value="{{ $Vouchers->date }}" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Payment mode <span class="text-danger">*</span></label>
                                                <div class="input-group">
                                                    <select class="form-control" name="payment_mode" required>
                                                        <option value="">---</option>
                                                        <option value="cash" {{($Vouchers->payment_mode == 'cash')? "selected": ""}}>Cash</option>
                                                        <option value="bank" {{($Vouchers->payment_mode == 'bank')? "selected": ""}}>Bank</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <label class="control-label m-t-20"for="example-input1-group2">Zakat<span class="text-danger">*</span></label>
                                                <div class="switch"  style="margin-top:20px" >
                                                    <label>NO
                                                        <input type="checkbox" value="1" name="zakat" {{($Vouchers->zakat == '1')? "checked": ""}}><span class="lever"></span>YES</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <label class="control-label m-t-20"for="example-input1-group2">Is Restricted?<span class="text-danger">*</span></label>
                                                <div class="switch"  style="margin-top:20px" >
                                                    <label>NO
                                                        <input type="checkbox" value="1" name="restricted" {{($Vouchers->restricted == '1')? "checked": ""}}><span class="lever"></span>YES</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <label class="control-label m-t-20" for="example-input1-group2">Restrictions Note<span class="text-danger">*</span></label>
                                                <div class="input-group">
                                                    <textarea class="form-control" rows="5" name="note" required>{{ $Vouchers->note }}</textarea>
                                                </div>
                                            </div>
                                        </div>
								<div class="row">
									<div class="col-lg-6 m-t-20">
										<div class="input-group">
											<button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
		<!-- ============================================================== -->
		<!-- End Page Content -->
		<!-- ============================================================== -->

	</div>


	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection
