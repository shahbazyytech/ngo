@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<div class="row page-titles">
			<div class="col-md-5 col-8 align-self-center">
				<h3 class="text-themecolor m-b-0 m-t-0">List Vouchers</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">List Vouchers</li>
				</ol>
			</div>
			
		</div>
		<!-- ============================================================== -->
		<!-- End Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
                        @endif

						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
						<h4 class="card-title">List Vouchers</h4>
						<div class="table-responsive m-t-40">
							<table id="myTable" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Name</th>
										<th>Amount</th>
										<th>Payment Mode</th>
										<th>Zakat</th>
										<th>Restricted</th>
										<th>Restrictions Note</th>
										<th class="text-nowrap">Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($Vouchers as $Voucher)
									<tr>
										<td>{{ $Voucher->name }}</td>
										<td>{{ $Voucher->amount }}</td>
										<td>{{ $Voucher->payment_mode }}</td>
										<td>{{ ($Voucher->zakat == 1)?'Yes':'No' }}</td>
										<td>{{ ($Voucher->restricted == 1)?'Yes':'No' }}</td>
										<td>{{ $Voucher->note }}</td>
										<td class="text-nowrap">
										<a href="{{ route('GetEditVouchers', $Voucher->id) }}" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
{{-- 									<a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a> --}}
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
	</div>

	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection
