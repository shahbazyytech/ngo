@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	     <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" id="app">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Collector</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Collector</li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
                            <h4 class="card-title">Collector</h4>
                            <div class="row">
									@if($errors->any())
									<div class="alert alert-danger">
										<ul>
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
		
								@if(session('success'))
									<div class="alert alert-success">
										<p>{{ session('success') }}</p>
									</div>
								@endif
                                <div class="col-sm-12 col-xs-12">
                                    <form action="{{ route('CreateCollector') }}" method="POST">
									    @csrf()
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Name<span class="text-danger">*</span></label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="name" placeholder="Enter Name" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">CNIC<span class="text-danger">*</span></label>
                                                <div class="input-group">
													<input type="number" class="form-control" name="cnic" data-mask="9999999999999" placeholder="Enter number without Dash ">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Family CNIC</label>
                                                <div class="input-group">
                                                    <input type="number" class="form-control" name="other_cnic" data-mask="9999999999999" placeholder="Enter number without Dash ">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Family size</label>
                                                <div class="input-group">
                                                    <input type="number" class="form-control" name="family_size" placeholder="Enter Family Size">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Contact Number<span class="text-danger">*</span></label>
                                                <div class="input-group">
                                                    <input type="number" class="form-control" name="contact_no" data-mask="9999999999" placeholder="03331234567" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
											<div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Source Of Income</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="source_of_income"  placeholder="Source Of Income">
                                                </div>
                                            </div>
											<div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Priority<span class="text-danger">*</span></label>
                                                <div class="input-group">
                                                    <select class="form-control" name="priority_id" required>
                                                        <option value="">---</option>
														@foreach ($Priorities as $priority)
															<option value='{{ $priority->id }}'>{{ $priority->name }}</option>
														@endforeach
                                                    </select>
                                                </div>
                                            </div>
										</div>
                                        <div class="row">
											<div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Need Rashan</label>
                                                <div class="input-group">
                                                    <select class="form-control" name="need_rashan">
                                                        <option value="">---</option>
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </div>
                                            </div>
											<div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Need Cash</label>
                                                <div class="input-group">
                                                    <select class="form-control" name="need_cash">
                                                        <option value="">---</option>
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
											<div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Reffered By</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="refferd_by" placeholder="Enter Reffered By">
                                                </div>
                                            </div>
											<div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Zakat <span class="text-danger">*</span></label>
                                                <div class="input-group">
                                                    <select class="form-control" name="zakat" required>
                                                        <option value="">---</option>
                                                        <option value="1">Accepted</option>
                                                        <option value="0">Not Accepted</option>
                                                    </select>
                                                </div>
                                            </div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Province<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" name="province_id" style="padding: 0;" v-model="selected_province" required>
													<option>Select Province</option>
														<option v-for="province in Provinces" :key="province.id" :value="province.id" >@{{ province.name }}</option>
													</select>
												</div>
											</div>
											<div  class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">City<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" name="city_id" style="padding: 0;" v-model="selected_city" required>
													<option>Select City</option>
														<option v-for="city in ProvinceCities" :key="city.id" :value="city.id" >@{{ city.city_name }}</option>
													</select>
												</div>
											</div>
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Area<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" name="area_id" style="padding: 0;">
													<option></option>
														<template v-for="Parea in CityAreas">
															<option :value="Parea.id" :key="Parea.id" disabled >@{{ Parea.name }}</option>
															<option v-for="area in Parea.inner_area" :value="area.id" :key="area.id" >@{{ area.name }}</option>
														</template>
													</select>
												</div>
											</div>
										</div>
                                        <div class="row">
											<div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Skills</label>
                                                <div class="input-group">
                                                    <textarea class="form-control" rows="5" name="skills"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Address<span class="text-danger">*</span></label>
                                                <div class="input-group">
                                                    <textarea class="form-control" rows="5" name="address" placeholder="Full address, Street, nearest landmark, area, district etc" required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 m-t-20">

												<div class="checkbox checkbox-primary pull-left p-t-0">
													<input id="checkbox-signup" name="collector_need" value="1" type="checkbox">
													<label for="checkbox-signup"> Register Needs </label>
												</div>

												<div class="input-group">
                                                    <button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection

@section('script')
<script>

   var App = new Vue({
	el: '#app',
	data: {
		selected_province: 0,
		selected_city: 0,
		Provinces: @json($Provinces, JSON_NUMERIC_CHECK),
		Cities: @json($Cities, JSON_NUMERIC_CHECK),
		Areas: @json($Areas, JSON_NUMERIC_CHECK)
	},
	computed: {
		ProvinceCities: function(){
			return this.Cities.filter( (city)	=>	{ return city.province_id == this.selected_province });
		},
		CityAreas: function(){
			return this.Areas.filter( (area)	=>	{ return area.city_id == this.selected_city });
		},
	}
   });
   
</script>
@endsection
