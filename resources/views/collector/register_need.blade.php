@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	     <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" id="app">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Register Needs</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Register Needs</li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
							@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
						<h4 class="card-title">{{ $Collector->name }}</h4>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form action="{{ route('RegisterNeed', $Collector->id) }}" method="POST">
									    @csrf()
                                        <div class="row">
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Required Rashan</label>
												<div class="input-group">
													<div class="checkbox checkbox-primary pull-left p-t-0">
														<input id="checkbox-signup" name="rashan" value="1" type="checkbox">
														<label for="checkbox-signup"> Required Rashan </label>
													</div>
												</div>
											</div>
											<div  class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Required Cash</label>
												<div class="input-group">
													<div class="checkbox checkbox-primary pull-left p-t-0">
														<input id="checkbox-signup2" name="cash" value="1" type="checkbox">
														<label for="checkbox-signup2"> Required Cash </label>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Priority <small><span class="text-danger">*</span></small></label>
												<div class="input-group">
												<select class="form-control" name="priority_id" style="padding: 0;">
													<option></option>
														<option v-for="priority in Priorities" :key="priority.id" :value="priority.id" >@{{ priority.name }}</option>
													</select>
												</div>
											</div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6 m-t-20">
                                                <div class="input-group">
                                                    <button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection

@section('script')
<script>

   var App = new Vue({
	el: '#app',
	data: {
		Priorities: @json($Priorities, JSON_NUMERIC_CHECK),
	},
	computed: {

	}
   });
   
</script>
@endsection
