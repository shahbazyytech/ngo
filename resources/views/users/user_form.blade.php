@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	     <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Users</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Users</li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
							<h4 class="card-title">User</h4>
							@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
                        @endif

						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form action="{{ route('CreateUser') }}" method="POST">
									    @csrf()
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Name<span class="text-danger">*</span></label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="name" placeholder="Enter Name" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Email<span class="text-danger">*</span></label>
                                                <div class="input-group">
													<input type="email" class="form-control" name="email" placeholder="Enter Email " required>
                                                </div>
                                            </div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<label class="control-label m-t-20" for="example-input1-group2">Password<span class="text-danger">*</span></label>
												<div class="input-group">
												<input type="password" class="form-control" name="password" required>
												</div>
											</div>
											<div class="col-lg-6">
												<label class="control-label m-t-20" for="example-input1-group2">Confirm Password<span class="text-danger">*</span></label>
												<div class="input-group">
													<input type="password" class="form-control" name="password_confirmation" required>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
                                                <label class="control-label m-t-20" for="example-input1-group2">Role</label>
                                                <div class="input-group">
                                                    <select class="form-control" name="role" required>
														<option></option>
                                                        <option  value="admin">Admin</option>
                                                        <option value="finance">Finance</option>
                                                        <option value="volunteer">Volunteer</option>
                                                    </select>
                                                </div>
                                            </div>
										</div>
                                        <div class="row">
                                            <div class="col-lg-6 m-t-20">
												<div class="checkbox checkbox-primary pull-left p-t-0">
													<input id="checkbox-signup" name="active" value="1" type="checkbox" checked>
													<label for="checkbox-signup"> Active </label>
												</div>
                                                <div class="input-group">
                                                    <button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection
