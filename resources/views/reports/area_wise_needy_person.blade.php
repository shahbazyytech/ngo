@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	     <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" id="app">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Area Wise Needy Persion Report</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Area Wise Needy Persion Report</li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
						@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
                            <h4 class="card-title">Select Area</h4>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form action="{{ route('AreaWiseNeedyPerson') }}" method="GET">
                                        <div class="row">
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Province<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" name="province_id" style="padding: 0;" v-model="selected_province" required>
													<option>Select Province</option>
														<option v-for="province in Provinces" :key="province.id" :value="province.id" >@{{ province.name }}</option>
													</select>
												</div>
											</div>
											<div  class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">City<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" name="city_id" style="padding: 0;" v-model="selected_city" required>
													<option>Select City</option>
														<option v-for="city in ProvinceCities" :key="city.id" :value="city.id" >@{{ city.city_name }}</option>
													</select>
												</div>
											</div>
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Area <small><span class="text-info"> (if-parent)</span></small></label>
												<div class="input-group">
												<select class="form-control" name="area_id" style="padding: 0;" v-model="selected_area">
													<option></option>
														<option v-for="area in CityAreas" :key="area.id" :value="area.id" >@{{ area.name }}</option>
													</select>
												</div>
											</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 m-t-20">
												<div class="checkbox checkbox-primary pull-left p-t-0">
													<input id="checkbox-signup" name="is_need" value="1" {{ Request::has('is_need')? 'checked' : '' }} type="checkbox">
													<label for="checkbox-signup"> Need Available </label>
												</div>
                                                <div class="input-group">
                                                    <button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Search</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
                <!-- /.row -->
                <!-- ============================================================== -->

                <div v-if="Area" class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
								<h4 class="card-title">List Need Person Reports of <u>@{{ Area.name }}</u></h4>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-sm table-bordered">
                                        <thead>
                                            <tr class="table-success">
                                                <th>Area Name</th>
                                                <th>Total No of Person</th>
												<th v-for="priority in Priorities">@{{ priority.name }}</th>
                                                <th>Total No of Volunteers</th>

                                            </tr>
                                        </thead>
                                        <tbody>
											<tr v-for="inner_area in Area.inner_area" class="table-active">
												<td>@{{ inner_area.name }}</td>
												<td>@{{ CountCollector(inner_area.id) }}</td>
                                                <td v-for="priority in Priorities">@{{ CountCollector(inner_area.id, priority.id) }}</td>
												<td>@{{ inner_area.volunteers.length }}</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr class="table-success">
                                                <td>Total</td>
												<td>@{{ Needs.length }}</td>
												<td v-for="priority in Priorities">@{{ CountCollector(null, priority.id) }}</td>
												<td>@{{ CountVolunteers }}</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

				<!-- End Page Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
			dom: 'Bfrtip',
			buttons: [
				'print'
			]
		});
    });
   var App = new Vue({
	el: '#app',
	data: {
		selected_province: {{ Request::input('province_id', 0) }},
		selected_city: {{ Request::input('city_id', 0) }},
		selected_area: {{ Request::input('area_id', 0) }},
		Area: @json($Area, JSON_NUMERIC_CHECK),
		Needs: @json($Needs, JSON_NUMERIC_CHECK),
		Priorities: @json($Priorities, JSON_NUMERIC_CHECK),
		Provinces: @json($Provinces, JSON_NUMERIC_CHECK),
		Cities: @json($Cities, JSON_NUMERIC_CHECK),
		Areas: @json($Areas, JSON_NUMERIC_CHECK)
	},
	computed: {
		ProvinceCities: function(){
			return this.Cities.filter( (city)	=>	{ return city.province_id == this.selected_province });
		},
		CityAreas: function(){
			return this.Areas.filter( (area)	=>	{ return area.city_id == this.selected_city });
		},
		CountVolunteers: function(){
			return	this.Area.inner_area.reduce((tot, area) => { return tot + area.volunteers.length }, 0);
		}
	},
	methods: {
		CountCollector: function(area_id = null, priority_id = null){
			if(area_id && priority_id){
				return this.Needs.filter((need) => { 
						return area_id == need.area_id && priority_id == need.priority_id;
						}).length;
			}
			if(area_id){
				return this.Needs.filter((need) => { 
						return area_id == need.area_id;
						}).length;
			}
			if(priority_id){
				return this.Needs.filter((need) => { 
						return priority_id == need.priority_id;
						}).length;
			}
		},
	}
   });
   
</script>
@endsection
