@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	     <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" id="app">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Inventory Report</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Inventory Report</li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
						@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
                            <h4 class="card-title">Select Date</h4>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form action="{{ route('InventoryReport') }}" method="GET">
                                        <div class="row">
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">From<span class="text-danger">*</span></label>
												<div class="input-group">
													<input type="date" value="{{ Request::input('date.from', date('Y-m-d')) }}" class="form-control" name="date[from]" style="padding: 0;" required/>
												</div>
											</div>
											<div  class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">To<span class="text-danger">*</span></label>
												<div class="input-group">
													<input type="date" value="{{ Request::input('date.to', date('Y-m-d')) }}" class="form-control" name="date[to]" style="padding: 0;" required/>
												</div>
											</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 m-t-20">
                                                <div class="input-group">
                                                    <button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Search</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
                <!-- /.row -->
                <!-- ============================================================== -->

				@if($Inventory)
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
								<h4 class="card-title">History</h4>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-sm table-bordered">
										<thead>
											<tr>
												<th>S.No</th>
												<th>Name</th>
												<th>Available</th>
												<th>Cost</th>
												<th>Date</th>
											</tr>
										</thead>
										<tbody>
											@foreach($Inventory AS $k=>$invent)
											<tr>
												<td>{{ $k+1 }}</td>
												<td>{{ $invent->item->name }}</td>
												<td>{{ $invent->available }} {{ $invent->item->unit }}</td>
												<td>{{ $invent->unit_cost.'*'.$invent->available }}={{ round(($invent->unit_cost*$invent->available), 2) }}</td>
												<td>{{ Carbon\Carbon::parse($invent->created_at)->toDateString() }}</td>
											</tr>
											@endforeach
										</tbody>
										<tfoot>
											<tr>
												<td>S.No</td>
												<td>Name</td>
												<td>Available</td>
												<td>Cost</td>
												<td>Date</td>
											</tr>
										</tfoot>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
				@endif
				<!-- End Page Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
			initComplete: function () {
            this.api().columns().every( function () {
					var column = this;
					var select = $('<select><option value=""></option></select>')
						.appendTo( $(column.footer()).empty() )
//                    .appendTo( $(column.header()).empty() )
						.on( 'change', function () {
							var val = $.fn.dataTable.util.escapeRegex(
								$(this).val()
							);

							column
								.search( val ? '^'+val+'$' : '', true, false )
								.draw();
						} );

					column.data().unique().sort().each( function ( d, j ) {
						select.append( '<option value="'+d+'">'+d+'</option>' )
					} );

			} );
		},
		dom: 'Blfrtip',
			buttons: [
				'print',
				'excel',

			],
		});
    });   
</script>
@endsection
