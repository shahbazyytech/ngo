@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	     <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" id="app">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Area Wise Volunteer Report</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Area Wise Volunteer Report</li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
						@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
                            <h4 class="card-title">Select Area</h4>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form action="{{ route('AreaWiseVolunteers') }}" method="GET">
                                        <div class="row">
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Province<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" name="province_id" style="padding: 0;" v-model="selected_province" required>
													<option>Select Province</option>
														<option v-for="province in Provinces" :key="province.id" :value="province.id" >@{{ province.name }}</option>
													</select>
												</div>
											</div>
											<div  class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">City<span class="text-danger">*</span></label>
												<div class="input-group">
												<select class="form-control" name="city_id" style="padding: 0;" v-model="selected_city" required>
													<option>Select City</option>
														<option v-for="city in ProvinceCities" :key="city.id" :value="city.id" >@{{ city.city_name }}</option>
													</select>
												</div>
											</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 m-t-20">
                                                <div class="input-group">
                                                    <button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Search</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
                <!-- /.row -->
                <!-- ============================================================== -->

                <div v-if="Areas" class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
								<h4 class="card-title">List Area Wise Volunteer of <u>@{{ city_name }}</u></h4>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-sm table-bordered">
										<template v-for="area in Areas">
											<thead>
												<tr class="table-active">
													<th colspan="6" class="text-center">@{{ area.name}}</th>
												</tr>                                            
												<tr>
													<th>S.No</th>
													<th>Name</th>
													<th>CNIC</th>
													<th>Age</th>
													<th>Contact Number</th>
													<th>Address</th>
												</tr>
											</thead>
											<tbody>
												<template v-for="inner_area in area.inner_area" v-if="inner_area.volunteers.length">
													<tr v-for="(volunteer, i) in inner_area.volunteers">
														<td>@{{ i+1 }}</td>
														<td>@{{ volunteer.name }}</td>
														<td>@{{ volunteer.cnic }}</td>
														<td>@{{ volunteer.age }}</td>
														<td>@{{ volunteer.contact_no }}</td>
														<td>@{{ volunteer.address }}</td>
													</tr>
												</template>
											</tbody>
										</template>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

				<!-- End Page Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
   var App = new Vue({
	el: '#app',
	data: {
		selected_province: {{ Request::input('province_id', 0) }},
		selected_city: {{ Request::input('city_id', 0) }},
		city_id: {{ Request::input('city_id', 0) }},
		Provinces: @json($Provinces, JSON_NUMERIC_CHECK),
		Cities: @json($Cities, JSON_NUMERIC_CHECK),
		Areas: @json($Areas, JSON_NUMERIC_CHECK),
	},
	computed: {
		ProvinceCities: function(){
			return this.Cities.filter( (city)	=>	{ return city.province_id == this.selected_province });
		},
		city_name: function(){
			return (this.Cities.find(city => city.id == this.city_id  )).city_name;
		}
	},
	methods: {

	}
   });
   
</script>
@endsection
