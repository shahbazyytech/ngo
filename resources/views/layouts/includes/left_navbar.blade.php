
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile" style="background: url({{ asset('assets/images/background/user-info.jpg') }} ) no-repeat;">
                    <!-- User profile image -->
                    <div class="profile-img"> <img src="{{ asset('assets/images/users/profile.png')}}" alt="user" /> </div>
                    <!-- User profile text-->
                    <!-- <div class="profile-text"> <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Markarn Doe</a>
                        <div class="dropdown-menu animated flipInY"> <a href="#" class="dropdown-item"><i class="ti-user"></i> My Profile</a> <a href="#" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a> <a href="#" class="dropdown-item"><i class="ti-email"></i> Inbox</a>
                            <div class="dropdown-divider"></div> <a href="#" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a>
                            <div class="dropdown-divider"></div> <a href="login.html" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a> </div>
                    </div> -->
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
						@hasrole('admin')
						<li>
							<a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><span class="hide-menu">Coverage Area</span></a>
							<ul aria-expanded="false" class="collapse">
								<li><a href="{{ route('AreaForm') }}">Add Area</a></li>
								<li><a href="{{ route('Area') }}">List of Areas</a></li>
							</ul>
						</li>
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><span class="hide-menu">Volunter </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('VolunteerForm') }}">Create Volunter</a></li>
                                <li><a href="{{ route('Volunteer') }}">List Volunter</a></li>
                            </ul>
						</li>
						@endhasrole
						@hasanyrole('admin|finance')
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><span class="hide-menu">Donors </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('InvestorsForm') }}">Add Donors</a></li>
                                <li><a href="{{ route('Investors') }}">List Donors</a></li>
                            </ul>
						</li>
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><span class="hide-menu">Donation Vouchers </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('VouchersForm') }}">Receive Vouchers</a></li>
                                <li><a href="{{ route('Vouchers') }}">List Donation Vouchers</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><span class="hide-menu">Inventory Management </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('ItemForm') }}">Add Items</a></li>
                                <li><a href="{{ route('InventoryVoucherForm') }}">Purchase/Issue Inventory</a></li>
                                <li><a href="{{ route('Items') }}">List Items</a></li>
                                <li><a href="{{ route('InventoryVouchers') }}">List Inventory Vouchers</a></li>
                            </ul>
						</li>
						@endhasanyrole
						@hasanyrole('admin|volunteer')
						<li>
							<a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><span class="hide-menu">Rashan Management</span></a>
							<ul aria-expanded="false" class="collapse">
								@hasrole('admin')
								<li><a href="{{ route('DonationPacketForm') }}">Create Rashan Packets</a></li>
								<li><a href="{{ route('DonationPackets') }}">List of Packets</a></li>
								<li><a href="{{ route('GetPacketsIssueVolunteer') }}">Issue To Volunteer</a></li>
								@endhasrole
								@hasanyrole('admin|volunteer')
								<li><a href="{{ route('GetPacketsIssueCollector') }}">Issue To Needy</a></li>
                                @endhasanyrole
							</ul>
						</li>
						@endhasanyrole
						@hasanyrole('admin|finance')
						<li>
                            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><span class="hide-menu">NGO Expenses</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('ExpensesForm') }}">Add Expense Type</a></li>
                                <li><a href="{{ route('Expenses') }}">List Expense</a></li>
                                <li><a href="{{ route('ExpenseVouchersForm') }}">Record Expenses Vocher</a></li>
                                <li><a href="{{ route('ExpenseVouchers') }}">List Expenses Vocher</a></li>
                            </ul>
						</li>
						@endhasanyrole
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><span class="hide-menu">Supplier/Shop/Home </span></a>
                            <ul aria-expanded="false" class="collapse">
								<li><a href="{{ route('SupplierForm') }}">Create Supplier</a></li>
								<li><a href="{{ route('Supplier') }}">List Supplier</a></li>
                            </ul>
						</li>
						@hasanyrole('admin|volunteer')
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><span class="hide-menu">Needy Person </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('CollectorForm') }}">Register</a></li>
                                <li><a href="{{ route('Collector') }}">List</a></li>
                            </ul>
						</li>
						@endhasanyrole
						@hasanyrole('admin|finance')
						<li>
							<a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><span class="hide-menu">Reports</span></a>
							<ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('AreaWiseNeedyPerson') }}">Area Wsie Needy Person</a></li>
								<li><a href="{{ route('AvailableNeedyPerson') }}">Available Needy Person</a></li>
								<li><a href="{{ route('AreaWiseVolunteers') }}">Area Wsie Volunteers</a></li>
								<li><a href="{{ route('VolunteerWiseDistribution') }}">Volunteer Wise Distribution</a></li>
								<li><a href="{{ route('InventoryReport') }}">Inventory</a></li>
							</ul>
						</li>
						@endhasanyrole
						@hasrole('admin')
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><span class="hide-menu">User</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('UserForm') }}">Add User</a></li>
                                <li><a href="{{ route('Users') }}">List User</a></li>
                            </ul>
						</li>
						@endhasrole
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>

