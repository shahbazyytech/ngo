@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<div class="row page-titles">
			<div class="col-md-5 col-8 align-self-center">
				<h3 class="text-themecolor m-b-0 m-t-0">Suppliers</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">Suppliers</li>
				</ol>
			</div>
		</div>
		<!-- .row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="card card-body">
					<h4 class="card-title">Suppliers/Shops/Homes where ration can be collected</h4>
					<div class="row">
						@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
                        @endif

						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
						<div class="col-sm-12 col-xs-12">
							<form method="post" action="{{ route('PostEditSupplier', $Supplier->id) }}">
								@csrf()
								<div class="row">
									<div class="col-lg-6">
										<label class="control-label m-t-20" for="example-input1-group2">Name of the Shop/Owner of the House</label>
										<div class="input-group">
											<input type="text" value="{{$Supplier->name}}" class="form-control" name="name">
										</div>
									</div>
									<div class="col-lg-6">
										<label class="control-label m-t-20" for="example-input1-group2">Quanity of Ration packs</label>
										<div class="input-group">
										<input type="text" value="{{ $Supplier->qty }}" class="form-control" name="qty">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<label class="control-label m-t-20" for="example-input1-group2">Type of ration packs</label>
										<div class="input-group">
											<input type="text" value="{{ $Supplier->type_of_rashan }}" class="form-control" name="type_of_rashan" placeholder="15 Days Ration, 1 Month Ration, Cloths, Vegs/Non veg items">
										</div>
									</div>
									<div class="col-lg-6">
										<label class="control-label m-t-20" for="example-input1-group2">Daily limit of supplies</label>
										<div class="input-group">
											<input type="text" value="{{ $Supplier->daily_supply_limit }}" class="form-control" name="daily_supply_limit">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<label class="control-label m-t-20" for="example-input1-group2">Timings of collection</label>
										<div class="input-group">
											<input type="text" value="{{ $Supplier->timing }}" class="form-control" name="timing">
										</div>
									</div>
									<div class="col-lg-6">
										<label class="control-label m-t-20" for="example-input1-group2">Contact Number</label>
										<div class="input-group">
											<input type="text" value="{{ $Supplier->contact_no }}" class="form-control" name="contact_no">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<label class="control-label m-t-20" for="example-input1-group2">Map Latitude</label>
										<div class="input-group">
											<input type="text" value="{{ $Supplier->map_lat }}" class="form-control" name="map_lat">
										</div>
									</div>
									<div class="col-lg-6">
										<label class="control-label m-t-20" for="example-input1-group2">Map Longitude</label>
										<div class="input-group">
											<input type="text" value="{{ $Supplier->map_lng }}" class="form-control" name="map_lng">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<label class="control-label m-t-20" for="example-input1-group2">Address</label>
										<div class="input-group">
										<textarea class="form-control" rows="5" name="address" placeholder="Full address with House/Shop Number, Street, nearest landmark, area, district etc">{{ $Supplier->address }}</textarea>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-6 m-t-20">
										<div class="input-group">
											<button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
		<!-- ============================================================== -->
		<!-- End Page Content -->
		<!-- ============================================================== -->

	</div>


	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection
