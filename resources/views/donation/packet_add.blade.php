@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	     <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" id="app">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
						<h3 class="text-themecolor m-b-0 m-t-0">List Donation Packets</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
							<li class="breadcrumb-item active">Update Donation Packets</li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
							@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
							<h4 class="card-title">List Packets</h4>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form action="{{ route('PostEditPacket', $DonationPacket->id) }}" method="POST">
									    @csrf()
                                        <div class="row">
											<div class="col-lg-6">
												<label class="control-label m-t-20" for="example-input1-group2">Name<span class="text-danger">*</span></label>
												<div class="input-group">
												<input type="text" class="form-control" name="name" placeholder="Enter Name" value="{{ $DonationPacket->name }}" readonly>
												</div>
											</div>
											<div class="col-lg-6">
												<label class="control-label m-t-20" for="example-input1-group2">Cash</label>
												<div class="input-group">
													<input type="number" class="form-control" placeholder="Cash" value="{{ $DonationPacket->cash }}" readonly>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">No Of Packets<span class="text-danger">*</span></label>
												<div class="input-group">
													<input type="text" class="form-control" placeholder="No Of Packet" value="{{ $DonationPacket->no_of_packets }}" readonly>
												</div>
											</div>
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Add Packets<span class="text-danger">*</span></label>
												<div class="input-group">
													<input type="number" class="form-control" placeholder="No Of Packet" v-model.number="add_packets" name="add_packets" required>
												</div>
											</div>
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Available Packets<span class="text-danger">*</span></label>
												<div class="input-group">
													<input type="text" class="form-control" placeholder="No Of Packet" v-model="available" readonly>
												</div>
											</div>
										</div>
										<br>
										<br>
										<br>
											<div class="container">
												<table class="table table-bordered table-striped">
													<thead>
														<tr>
															<th>Name</th>
															<th>Value</th>
														</tr>
													</thead>
													<tbody>
														<tr v-for="(packet, i) in rashan">
															<td><input class="form-control" type="text" v-model="packet.name" readonly  > </td>
															<td><input class="form-control" type="text" v-model="packet.value" readonly > </td>
														</tr>
													</tbody>
												</table>
											</div>

                                        <div class="row">
                                            <div class="col-lg-6 m-t-20">
                                                <div class="input-group">
                                                    <button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection

@section('script')
<script>

   var App = new Vue({
	el: '#app',
	data: {
		add_packets: 0,
		rashan: @json($DonationPacket->rashan, JSON_NUMERIC_CHECK),
	},
	computed: {
		available: function(){
			return this.add_packets + {{ $DonationPacket->available }};
		},
	}
   });
   
</script>
@endsection
