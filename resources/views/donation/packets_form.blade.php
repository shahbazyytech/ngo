@extends('layouts.master')

@section('content')
	<!-- ============================================================== -->
	     <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" id="app">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
						<h3 class="text-themecolor m-b-0 m-t-0">List Donation Packets</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
							<li class="breadcrumb-item active">Add Donation Packets</li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-body">
							@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						@if(session('success'))
							<div class="alert alert-success">
								<p>{{ session('success') }}</p>
							</div>
						@endif
							<h4 class="card-title">List Packets</h4>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form action="{{ route('CreatePacket') }}" method="POST" @submit="submitForm">
									    @csrf()
                                        <div class="row">
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Name<span class="text-danger">*</span></label>
												<div class="form-group">
													<input type="text" class="form-control" name="name" placeholder="Enter Name" required>
												</div>
											</div>
											<div class="col-lg-4 d-none">
												<label class="control-label m-t-20" for="example-input1-group2">Cash</label>
												<div class="form-group">
													<input type="number" class="form-control" name="cash" placeholder="Cash">
												</div>
											</div>
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">No Of Packets<span class="text-danger">*</span></label>
												<div class="form-group">
													<input type="number" @change="calculated=false" class="form-control" v-model.number="no_of_packets" min="1" name="no_of_packets" placeholder="No Of Packet" required>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Mazdoori<span class="text-danger">*</span></label>
												<div class="form-group">
													<input type="number" class="form-control" v-model.number="mazdoori" name="mazdoori" placeholder="Mazdoori" >
												</div>
											</div>
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Transportation<span class="text-danger">*</span></label>
												<div class="form-group">
													<input type="number" class="form-control" v-model.number="transportation" name="transportation" placeholder="Transportation" >
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<label class="control-label m-t-20" for="example-input1-group2">Add Item</label>
												<div class="input-group">
													<select class="form-control" v-model="SelectedItem" >
														<option v-for="Item in Items" :value="Item" >@{{ Item.name+' | '+Item.qty+' '+Item.unit }}</option>
													</select>
													<button type="button" @click="addRashan" class="btn btn-primary" ><span class="fa fa-plus"></span></button>
												</div>
											</div>
										</div>
										<br>
										<br>
										<br>
											<div class="container">
												<table class="table table-bordered table-striped">
													<thead>
														<tr>
															<th>Name</th>
															<th>Qty</th>
															<th>Unit Cost</th>
															<th>Total</th>
															<th>
																Remain Qty
																<br>
																(availableQty-(inputQty*no_of_packets))
															</th>
															<th>Option</th>
														</tr>
													</thead>
													<tbody>
														<tr v-for="(packet, i) in rashan">
															<td>
																<input class="form-control" type="hidden" :name="'rashan['+i+'][name]'" :value="packet.name" required="true" readonly >
																<input class="form-control" type="hidden" :name="'rashan['+i+'][item_id]'" :value="packet.id" required="true" readonly >
																@{{ packet.name }}
															</td>
															<td>
																<div class="input-group">
																	<input class="form-control" type="number" :name="'rashan['+i+'][value]'" @change="calculated=false" min="1" :max="(packet.qty*no_of_packets)" v-model.number="packet.input_qty" required="true" >
																	<span class="form-control" style="width: 10px" >@{{ packet.unit }}</span>
																</div>
															</td>
															<td>
																@{{ packet.avg_cost }}
																<input class="form-control" type="hidden" :name="'rashan['+i+'][unit_cost]'" :value="packet.avg_cost" required="true" readonly >
																<input class="form-control" type="hidden" :name="'rashan['+i+'][unit]'" :value="packet.unit" required="true" readonly >
															</td>
															<td>
																@{{ (packet.input_qty*packet.avg_cost).toFixed(3) }}
															</td>
															<td>
																<div class="input-group">
																	<input class="form-control" type="number" :name="'rashan['+i+'][remain_qty]'" min="0" :value="(packet.qty-(packet.input_qty*no_of_packets))" required="true" readonly="true" >
																	<span class="form-control" style="width: 10px" >@{{ packet.unit }}</span>
																</div>
															</td>
															<td><a class="btn btn-sm" @click="rashan.splice(i,1)"><i class="fa fa-remove"></i></a></td>
														</tr>
													</tbody>
													<tfoot>
														<tr>
															<th>
																<button v-show="rashan.length" type="button" @click="Calculate" class="btn btn-primary" :disabled="calculated">Calculate</button>
															</th>
															<th colspan="2" class="text-right">Total</th>
															<td>
																<input type="hidden" name="unit_cost" :value="TotalAmount">
																@{{ TotalAmount }} Rs 
															</td>
															<td>
																@{{ AllTotalAmount }} Rs
															</td>
														</tr>
													</tfoot>
												</table>
											</div>

                                        <div class="row">
                                            <div class="col-lg-6 m-t-20">
                                                <div class="input-group" v-if="rashan.length && calculated">
                                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
@endsection

@section('script')
<script>

   var App = new Vue({
	el: '#app',
	data: {
		Items: @json($Items, JSON_NUMERIC_CHECK),
		SelectedItem: null,
		rashan: [],
		no_of_packets: 0,
		mazdoori: 0,
		transportation: 0,
		calculated: false,
	},
	computed: {
		TotalAmount: function(){
			return this.rashan.reduce((t, v) => (t + (v.input_qty * v.avg_cost)), 0.000).toFixed(2);
		},
		AllTotalAmount: function(){
			return (((this.TotalAmount*this.no_of_packets)+this.mazdoori)+this.transportation).toFixed(2);
		}
	},
	watch: {

	},
	methods: {
		addRashan: function(){
			if(this.SelectedItem){
				let find = this.rashan.find(packet => packet.id == this.SelectedItem.id);
				if(find == null){
					this.rashan.push({ ...this.SelectedItem, input_qty: 0, avg_cost: 0});
					this.calculated = false;
				} else {
					alert("Item already selected");
				}
			} else {
				alert("Select Item");
			}
		},
		Calculate: function(){

			if(this.no_of_packets){

				let data = this.rashan.map((row) => { return {id: row.id, input_qty: (row.input_qty*this.no_of_packets)}; })

				$.ajax({
					method: "GET",
					url: "{{ route('CalculateForDonationPacket') }}",
					data: { data }
				})
				.done(function( res ) {
					App.calculated = true;
					App.rashan.forEach((row, k) => {
						cost =	res.data.find((r) => row.id==r.id);
						App.rashan[k].avg_cost = cost.avg_cost;
					});
				});

			}

		},
		submitForm: function(e){

			var error = null;
			this.rashan.forEach((row) => {
				if(error == null){
					if((row.input_qty*App.no_of_packets) > row.qty){
						error =	'invalid Qty';
					}
				}
			});

			if(error){
				alert(error);
				e.preventDefault();
			}


		}
	}
   });
   
</script>
@endsection
